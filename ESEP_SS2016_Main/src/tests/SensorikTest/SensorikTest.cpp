/*
 * SensorikTest.cpp
 *
 *  Created on: Apr 27, 2016
 *      Author: Mir Farshid Baha
 */

#include <iostream>
#include <sstream>
#include <string>
#include <unistd.h>

#include "../../lib/hal/Actuators.h"
#include "../../lib/hal/Sensors.h"
#include "../../lib/hal/portmap.h"
#include "SensorikTest.h"

using namespace Test;
using namespace HAL;
using namespace std;

SensorikTest* SensorikTest::instance_ = NULL;

void SensorikTest::sensor_handler(struct _pulse* pulse, string* msg, Sensors::Source in, Sensors* sensors) {
    do {
        MsgReceivePulse(sensors->get_isrChannel(), pulse, sizeof(struct _pulse), NULL);
        if (sensors->msgDecoder(pulse->value.sival_int) == in) {
            cout << *msg << "\n" << endl;
            break;
        }
    } while (1);
}

void SensorikTest::start() {
    ThreadCtl(_NTO_TCTL_IO_PRIV, 0);
    out8(0x303, 0b10001010);

    Sensors* sensors = Sensors::getInstance();
    struct _pulse pulse;
    char input = '0';
    bool running = true;
    string msg = "";

    cout << "Welcome to the interactive HAL-sensoric elements'test" << endl;
    cout << "Please press 1-9 or h to choose a test, press q to quit" << endl;

    cout << " Press 1: LIGHTBARRIER_ENTRY" << endl;
    cout << "Press 2: LIGHTBARRIER_HEIGHT_MEASUREMENT" << endl;
    cout << "Press 3: LIGHTBARRIER_SWITCH" << endl;
    cout << "Press 4: LIGHTBARRIER_RAMP" << endl;
    cout << "Press 5: LIGHTBARRIER_EXIT" << endl;
    cout << "Press 6: BUTTON_START" << endl;
    cout << "Press 7: BUTTON_STOP" << endl;
    cout << "Press 8: BUTTON_RESET" << endl;
    cout << "Press 9: BUTTON_E_STOP" << endl;
    cout << "Press m: METAL_DETECTOR" << endl;
    cout << "Press h: HEIGHT_MEASUREMENT" << endl;
    cout << "Press q: QUIT" << endl;

    while (running) {
        input_handler(&input);
        switch (input) {
            case '1':
                cout << "Please breach LIGHTBARRIER_ENTRY.." << endl;
                msg = "LIGHTBARRIER_ENTRY breached";
                SensorikTest::sensor_handler(&pulse, &msg, Sensors::lightbarrier_entry, sensors);
                break;

            case '2':
                cout << "Please breach LIGHTBARRIER_HEIGHT_MEASUREMENT.." << endl;
                msg = "LIGHTBARRIER_HEIGHT_MEASUREMENT breached";
                SensorikTest::sensor_handler(&pulse, &msg, Sensors::lightbarrier_height_measurement, sensors);
                break;

            case '3':
                cout << "Please breach LIGHTBARRIER_SWITCH.." << endl;
                msg = "LIGHTBARRIER_SWITCH breached";
                SensorikTest::sensor_handler(&pulse, &msg, Sensors::lightbarrier_switch, sensors);
                break;
            case 'm':
                cout << "Please place metal under METAL_DETECTOR.." << endl;
                msg = "METALL_DETECTOR is alive oO";
                SensorikTest::sensor_handler(&pulse, &msg, Sensors::metall_detector, sensors);
                break;
            case '4':

                cout << "Please breach LIGHTBARRIER_RAMP.." << endl;
                msg = "LIGHTBARRIER_RAMP breached";
                SensorikTest::sensor_handler(&pulse, &msg, Sensors::lightbarrier_ramp, sensors);
                break;
            case '5':
                cout << "Please breach LIGHTBARRIER_EXIT.." << endl;
                msg = "LIGHTBARRIER_EXIT breached";
                SensorikTest::sensor_handler(&pulse, &msg, Sensors::lightbarrier_exit, sensors);
                break;
            case '6':
                cout << "Please press BUTTON_START.." << endl;
                msg = "BUTTON_START pressed";
                SensorikTest::sensor_handler(&pulse, &msg, Sensors::button_start, sensors);
                break;
            case '7':
                cout << "Please press BUTTON_STOP.." << endl;
                msg = "BUTTON_STOP pressed";
                SensorikTest::sensor_handler(&pulse, &msg, Sensors::button_stop, sensors);
                break;
            case '8':
                cout << "Please press BUTTON_RESET.." << endl;
                msg = "BUTTON_RESET pressed";
                SensorikTest::sensor_handler(&pulse, &msg, Sensors::button_reset, sensors);
                break;
            case '9':
                cout << "Please pull BUTTON_E_STOP.." << endl;
                msg = "BUTTON_E_STOP pulled";
                SensorikTest::sensor_handler(&pulse, &msg, Sensors::button_e_stop, sensors);
                break;
            case 'h':
                cout << "Height Measurement test starts here" << endl;
                cout << "Current Height: " << (sensors->getSensorHeight()) << endl;
                break;
            case 'q':
                running = false;
                cout << "End of the SensorikTest" << endl;
                break;
            default:
                break;
        }
    }
}

void SensorikTest::input_handler(char* in) {
    string input = "";
    int inputLength = 0;
    while (true) {
        getline(cin, input);
        inputLength = input.length();
        if (inputLength == 1 &&
            ((48 < input[0] && input[0] < 58) || input[0] == 'h' || input[0] == 'q' || input[0] == 'm')) {
            *(in) = input[0];
            break;
        }
        cout << "Invalid command, please type 1-9, h or q" << endl;
    }
}

SensorikTest::SensorikTest() {}
SensorikTest::~SensorikTest() {}

SensorikTest* SensorikTest::getInstance() {
    static SensorikTest instance_;
    return &instance_;
}
