/**
 *  @file SensorikTest.h
 *  @date 27.04.2016
 *  @version 1.0
 *
 *  @author Mir Farshid Baha
 */

#ifndef SENSORIKTEST_H_
#define SENSORIKTEST_H_
#include "../../lib/hal/Sensors.h"
#include <string>
using namespace HAL;
/**
 * This namespace writes all test to be performed on the system.
 */
namespace Test {
    //! Header for interactive unit test class for the sensors.
    class SensorikTest {
    private:
        //! Instance of SensorikTest Object for Singleton.
        static SensorikTest* instance_;

        SensorikTest();
        SensorikTest(const SensorikTest& other);
        SensorikTest& operator=(const SensorikTest& other);
        ~SensorikTest();

        void sensor_handler(struct _pulse*, std::string*, Sensors::Source, Sensors* sensors);
        void input_handler(char*);

    public:
        //! Get instance of Singleton object.
        static SensorikTest* getInstance();
        //! Start interactive unit test.
        void start();
    };
}

#endif /* SENSORIKTEST_H_ */
