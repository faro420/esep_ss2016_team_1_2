/*
 * InteractiveTest.cpp
 *
 *  Created on: Apr 9, 2016
 *      Author: Mir Farshid Baha
 */

#include <iostream>
#include <sstream>
#include <string>
#include <unistd.h>

#include "../../lib/hal/Actuators.h"
#include "../../lib/hal/portmap.h"
#include "InteractiveTest.h"

using namespace Test;
using namespace HAL;
using namespace std;

InteractiveTest* InteractiveTest::instance_ = NULL;

void InteractiveTest::testMotor() {
    char input = '0';
    Actuators* mt = Actuators::getInstance();
    cout << "Welcome to the Motor test" << endl;

    cout << "Moving fast forwards" << endl;
    mt->motorStart(Actuators::FORWARDS, Actuators::FAST);

    input_handler(&input);
    cout << "Moving fast backwards" << endl;
    mt->motorStart(Actuators::BACKWARDS, Actuators::FAST);

    input_handler(&input);
    cout << "Moving slowly forwards" << endl;
    mt->motorStart(Actuators::FORWARDS, Actuators::SLOW);

    input_handler(&input);
    cout << "Moving slowly backwards" << endl;
    mt->motorStart(Actuators::BACKWARDS, Actuators::SLOW);

    input_handler(&input);
    mt->motorStop();
    cout << "End of the Motor Test" << endl;
}

void InteractiveTest::testTrafficLight() {
    Actuators* tl = Actuators::getInstance();
    char input = '0';
    cout << "Welcome to the traffic light test" << endl;

    cout << "Green Led is going to turn on then off" << endl;
    tl->trafficLightOn(Actuators::GREEN);
    input_handler(&input);
    tl->trafficLightOff(Actuators::GREEN);

    cout << "Amber Led is going to turn on then off" << endl;
    tl->trafficLightOn(Actuators::AMBER);
    input_handler(&input);
    tl->trafficLightOff(Actuators::AMBER);

    cout << "Red Led is going to turn on the off" << endl;
    tl->trafficLightOn(Actuators::RED);
    input_handler(&input);
    tl->trafficLightOff(Actuators::RED);

    cout << "End of the TrafficLight Test" << endl;
}

void InteractiveTest::testSwitch() {
    Actuators* sw = Actuators::getInstance();
    char input = '0';
    cout << "Welcome to the switch test" << endl;

    cout << "The Switch will open now" << endl;
    sw->switchOpen();
    input_handler(&input);
    cout << "The Switch will close now" << endl;
    sw->switchClose();

    cout << "End of the Switch Test" << endl;
}

void InteractiveTest::testButtonLED() {
    Actuators* sw = Actuators::getInstance();
    char input = '0';
    cout << "Welcome to the button led test" << endl;

    cout << "The start button LED will be turned on now" << endl;
    sw->buttonLightOn(Actuators::START);
    input_handler(&input);
    sw->buttonLightOff(Actuators::START);

    cout << "The reset button LED will be turned on now" << endl;
    sw->buttonLightOn(Actuators::RESET);
    input_handler(&input);
    sw->buttonLightOff(Actuators::RESET);

    cout << "The Q1 button LED will be turned on now" << endl;
    sw->buttonLightOn(Actuators::Q1);
    input_handler(&input);
    sw->buttonLightOff(Actuators::Q1);

    cout << "The Q2 button LED will be turned on now" << endl;
    sw->buttonLightOn(Actuators::Q2);
    input_handler(&input);
    sw->buttonLightOff(Actuators::Q2);

    cout << "End of the button led Test" << endl;
}

void InteractiveTest::start() {
    ThreadCtl(_NTO_TCTL_IO_PRIV, 0);
    out8(0x303, 0b10001010);

    cout << "Welcome to the interactive HAL-actuating elements'test" << endl;
    cout << "Please press p each time to proceed to the next part of the test" << endl;
    char input = '0';
    input_handler(&input);
    testMotor();
    cout << "Please press p to proceed" << endl;
    input_handler(&input);
    testTrafficLight();
    cout << "Please press p to proceed" << endl;
    input_handler(&input);
    testSwitch();
    cout << "Please press p to proceed" << endl;
    input_handler(&input);
    testButtonLED();
    cout << "End of the Interactive Test" << endl;
}
void InteractiveTest::input_handler(char* in) {
    string input = "";
    int inputLength = 0;
    while (true) {
        getline(cin, input);
        inputLength = input.length();
        if (inputLength == 1 && (input[0] == 'P' || input[0] == 'p')) {
            *(in) = input[0];
            break;
        }
        cout << "Invalid command, please type p to proceed" << endl;
    }
}

InteractiveTest::InteractiveTest() {}
InteractiveTest::~InteractiveTest() {}

InteractiveTest* InteractiveTest::getInstance() {
    static InteractiveTest instance_;
    return &instance_;
}
