/**
 *  @file InteractiveTest.h
 *  @date 06.04.2016
 *  @version 1.0
 *
 *  @author Mir Farshid Baha
 */

#ifndef INTERACTIVETEST_H_
#define INTERACTIVETEST_H_
/**
* This namespace writes all test to be performed on the system.
*/
namespace Test {
    //! Header for interactive unit test class for the actuators.
    class InteractiveTest {
    private:
        //! Instance of InteractiveTest Object for Singleton.
        static InteractiveTest* instance_;

        InteractiveTest();
        InteractiveTest(const InteractiveTest& other);
        InteractiveTest& operator=(const InteractiveTest& other);
        ~InteractiveTest();
        //! Tests all features of the motor.
        void testMotor();
        //! Test all features of traffic light.
        void testTrafficLight();
        //! Test functionality of the switch.
        void testSwitch();
        //! Test functionality of the button leds.
        void testButtonLED();
        /** @param[in] Character from user input.
        *
        */
        void input_handler(char*);

    public:
        //! Get instance of Singleton object.
        static InteractiveTest* getInstance();
        //! Start interactive unit test.
        void start();
    };
}

#endif /* INTERACTIVETEST_H_ */
