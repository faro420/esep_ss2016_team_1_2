/** @mainpage ESEP_SS2016_Team_1_2 Documentation
 * @section sec1 Introduction
 * This is the documentation of the ESEP_SS2016 Project.
 *
 *
 *
 *  @date 06.04.2016
 *  @version 1.0
 *  As of now only the interactive test for the actuators is executed.
 *  @author Mir Farshid Baha
 */

#include "InteractiveTest.h"
using namespace Test;

int main() {
    InteractiveTest* test = InteractiveTest::getInstance();
    test->start();
    return 0;
}
