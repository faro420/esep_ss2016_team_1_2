/** @mainpage ESEP_SS2016_Team_1_2 Documentation
 * @section sec1 Introduction
 * This is the documentation of the ESEP_SS2016 Project.
 *
 *
 *
 *  @date 06.04.2016
 *  @version 1.0
 *  As of now only the interactive test for the actuators is executed.
 *  @author Mir Farshid Baha
 */
//#include "tests/SensorikTest/SensorikTest.h"
#include "../timer/Timer.h"
#include "lib/Logger.h"
#include "lib/SerialListener.h"
#include "lib/SerialcomTest.h"
#include "lib/hal/Actuators.h"
#include "lib/hsm/Demultiplexer.h"

//#include <iostream>
using namespace std;
using namespace Logger;
// using namespace HAL;
// using namespace Test;

int main() {
    Demultiplexer::getInstance()->start(NULL);
    Actuators::getInstance()->trafficLightOff(Actuators::GREEN);
    Actuators::getInstance()->trafficLightOn(Actuators::AMBER);
    while (!Actuators::getInstance()->isInInitState()) {
        usleep(100000);
    }
    Actuators::getInstance()->trafficLightOff(Actuators::AMBER);
    Actuators::getInstance()->trafficLightOn(Actuators::GREEN);

    for (int i = 0; i < 3; i++) {
        Actuators::getInstance()->trafficLightOn(Actuators::AMBER);
        usleep(150 * 1000);
        Actuators::getInstance()->trafficLightOn(Actuators::RED);
        usleep(150 * 1000);
        Actuators::getInstance()->trafficLightOff(Actuators::RED);
        usleep(150 * 1000);
        Actuators::getInstance()->trafficLightOff(Actuators::AMBER);
        usleep(150 * 1000);
    }

    cout << timestamp << "starting..." << endl;

    Demultiplexer::getInstance()->init = false;
    Actuators::getInstance()->start(NULL);
    SerialListener::getInstance()->start(NULL);
    Timer::getInstance()->start(NULL);

    Actuators::getInstance()->join();
    Timer::getInstance()->join();
    SerialListener::getInstance()->join();
    Demultiplexer::getInstance()->join();

    //	struct _clockperiod cp;
    //	ClockPeriod(CLOCK_REALTIME, 0, &cp, 0);
    //
    //	cout << "fract: " << cp.fract << " ns: " << cp.nsec << endl;
}

/*
int main() {
    SensorikTest* test = SensorikTest::getInstance();
    test->start();
    return 0;
}
*/
