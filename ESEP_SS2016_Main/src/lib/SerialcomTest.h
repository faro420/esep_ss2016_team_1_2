/*
 *author: Mir Farshid Baha
 *contact: mirfarshid.baha@haw-hamburg.de
 *version: 1.0
 */

#ifndef SERIALCOMTEST_H_
#define SERIALCOMTEST_H_

#include "SerialCom.h"

namespace Test {
    class SerialcomTest {
    private:
        static SerialcomTest* instance_;

        SerialcomTest();
        SerialcomTest(const SerialcomTest& other);
        SerialcomTest& operator=(const SerialcomTest& other);
        ~SerialcomTest();

    public:
        static SerialcomTest* getInstance();
        void start();
    };
}
#endif /* SERIALCOMTEST_H_ */
