/**
 *  @file Timer.h
 *  @date 27.04.2016
 *  @version 1.0
 *  @author Dennis Kirsch
 */

#ifndef TIMER_H_
#define TIMER_H_

#include "../HAWThread.h"
#include <vector>

using namespace thread;
class Timer : public HAWThread {
private:
    struct timerdata {
        timer_t id;
        struct itimerspec timerspec;
        bool isInterruptable;
    };

    vector<timerdata> timers;

    int connectionID;
    int channelID;

    static Timer* instance_;

    Timer();
    virtual ~Timer();

public:
    enum TimerType { HEARTBEAT, SWITCH, RAMP, PUCK_TICK, RESET };

    //! Get instance of Timer object Singleton.
    static Timer* getInstance();

    /** Creates a new timer.
    * @param[in] int The interval in seconds in which the timer will send a signal.
    * @param[in] int The interval in nanoseconds in which the timer will send a signal.
    * @param[in] struct sigevent* Event, which will be triggered from timer. If NULL it will be processed by
    * the default message queue.
    * @param[in] bool Signals if the timer can be interrupted (by stopTimer()) or not.
    * @param[in] bool Signals if the timer should be repeated or not.
    * @return timer_t The timer ID of the generated timer.
    */
    timer_t createTimer(TimerType type, int intervalInS, int intervalInNs, struct sigevent* event,
                        bool isInterruptable, bool repeat);

    /** Stops the given timer.
    * @param[in] timer_t The timer ID of the timer which will be stopped.
    */
    int stopTimer(timer_t);

    void deleteTimer(timer_t);

    void modifyTimer(timer_t timerID, int intervalInS, int intervalInNs, bool repeat);

    //! Start all timers which were stopped by stopAllTimers().
    void startTimer(timer_t);

    //! Stops all timers which are interruptable (see createTimer()).
    void stopAllTimers();

    //! Start all timers which were stopped by stopAllTimers().
    void startAllTimers();

    //! Inherited by HAWThread
    virtual void execute(void*);

    //! Inherited by HAWThread
    virtual void shutdown();
};

#endif /* TIMER_H_ */
