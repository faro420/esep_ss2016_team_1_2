/**
 *  @file Timer.cpp
 *  @date 27.04.2016
 *  @version 1.0
 *  @author Dennis Kirsch
 */

#include "Timer.h"
#include "../hal/Sensors.h"
#include "../hsm/Dispatcher.h"
#include <cstring>
#include <stdlib.h>
#include <sys/neutrino.h>
#include <time.h>

#define VECTOR_LENGTH 100

using namespace std;

Timer::Timer() : connectionID(0), channelID(0) {
    if (ThreadCtl(_NTO_TCTL_IO_PRIV, 0) == -1) {
        exit(EXIT_FAILURE);
    }

    if ((channelID = ChannelCreate(0)) == -1) {
        exit(EXIT_FAILURE);
    }
    if ((connectionID = ConnectAttach(0, 0, HAL::Sensors::getInstance()->get_isrChannel(), 0, 0)) == -1) {
        exit(EXIT_FAILURE);
    }

    vector<timerdata> timers(VECTOR_LENGTH);
}

Timer::~Timer() {
    if (ConnectDetach(connectionID) == -1) {
        exit(EXIT_FAILURE);
    }

    if (ChannelDestroy(channelID) == -1) {
        exit(EXIT_FAILURE);
    }
}

Timer* Timer::getInstance() {
    static Timer instance_;

    return &instance_;
}

timer_t Timer::createTimer(TimerType type, int intervalInS, int intervalInNs, struct sigevent* event,
                           bool isInterruptable, bool repeat) {
    timer_t timerID;
    struct itimerspec timerSpec;
    struct timerdata timerData;

    struct sigevent eventToSend;

    if (event == NULL) {
        SIGEV_PULSE_INIT(&eventToSend, connectionID, SIGEV_PULSE_PRIO_INHERIT, TIMER, type);
    } else {
        eventToSend = *event;
    }

    if (timer_create(CLOCK_REALTIME, &eventToSend, &timerID) == -1) {
        return -1;
    }

    timerData.id = timerID;
    timerData.isInterruptable = isInterruptable;

    timerSpec.it_value.tv_sec = intervalInS;
    timerSpec.it_value.tv_nsec = intervalInNs;
    timerSpec.it_interval.tv_sec = 0;
    timerSpec.it_interval.tv_nsec = 0;
    if (repeat) {
        timerSpec.it_interval.tv_sec = intervalInS;
        timerSpec.it_interval.tv_nsec = intervalInNs;
    }

    memcpy(&(timerData.timerspec), &timerSpec, sizeof(timerSpec));

    timer_settime(timerID, 0, &timerSpec, NULL);

    timers.push_back(timerData);

    return timerID;
}

void Timer::modifyTimer(timer_t timerID, int intervalInS, int intervalInNs, bool repeat) {
    for (vector<timerdata>::iterator it = timers.begin(); it != timers.end(); ++it) {
        if (it->id == timerID) {
            struct itimerspec timeIn;
            timeIn.it_value.tv_sec = intervalInS;
            timeIn.it_value.tv_nsec = intervalInNs;
            timeIn.it_interval.tv_sec = 0;
            timeIn.it_interval.tv_nsec = 0;
            if (repeat) {
                timeIn.it_interval.tv_sec = intervalInS;
                timeIn.it_interval.tv_nsec = intervalInNs;
            }

            if (timer_settime(it->id, 0, &timeIn, 0) == -1) {
                continue;
            }

            return;
        }
    }
}

int Timer::stopTimer(timer_t timerID) {
    bool found;

    found = false;
    for (vector<timerdata>::iterator it = timers.begin(); it != timers.end(); ++it) {
        if (it->id == timerID) {
            struct itimerspec timeOut;
            struct itimerspec timeIn;
            timeIn.it_value.tv_sec = 0;
            timeIn.it_value.tv_nsec = 0;
            timeIn.it_interval.tv_sec = 0;
            timeIn.it_interval.tv_nsec = 0;

            if (timer_settime(it->id, 0, &timeIn, &timeOut) == -1) {
                continue;
            }

            it->timerspec = timeOut;
            break;
        }
    }

    if (!found) {
        return -1;
    }

    return 0;
}

void Timer::deleteTimer(timer_t id) {
    for (vector<timerdata>::iterator it = timers.begin(); it != timers.end(); ++it) {
        if (it->id == id) {
            cout << "TIMER: timer_delete id:" << id << endl;
            timer_delete(id);
            timers.erase(it);
            break;
        }
    }
}

void Timer::startTimer(timer_t timerID) {
    for (vector<timerdata>::iterator it = timers.begin(); it != timers.end(); ++it) {
        if (it->id == timerID) {
            timer_settime(it->id, 0, &(it->timerspec), NULL);
            cout << "timer id " << it->id << ": ns1 " << it->timerspec.it_value.tv_nsec << ", ns2 "
                 << it->timerspec.it_interval.tv_nsec << endl;
            return;
        }
    }
}

void Timer::stopAllTimers() {
    for (vector<timerdata>::iterator it = timers.begin(); it != timers.end(); ++it) {
        if (it->isInterruptable) {
            struct itimerspec timeOut;
            struct itimerspec timeIn;
            timeIn.it_value.tv_sec = 0;
            timeIn.it_value.tv_nsec = 0;
            timeIn.it_interval.tv_sec = 0;
            timeIn.it_interval.tv_nsec = 0;

            if (timer_settime(it->id, 0, &timeIn, &timeOut) == -1) {
                continue;
            }

            memcpy(&(it->timerspec), &timeOut, sizeof(itimerspec));
        }
    }
}

void Timer::startAllTimers() {
    for (vector<timerdata>::iterator it = timers.begin(); it != timers.end(); ++it) {
        if (it->isInterruptable) {
            timer_settime(it->id, 0, &(it->timerspec), NULL);
        }
    }
}

void Timer::execute(void*) {
    struct _pulse pulseMsg;

    while (1) {
        MsgReceivePulse(channelID, &pulseMsg, sizeof(pulseMsg), NULL);
        Dispatcher::getInstance()->callListeners(UPDATE_TIMER);
    }
}

void Timer::shutdown() {
    // räume auf
}
