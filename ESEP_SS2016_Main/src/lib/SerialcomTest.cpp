/*
 *author: Mir Farshid Baha, Darryl Imhof
 *contact: mirfarshid.baha@haw-hamburg.de, darryl.imhof@haw-hamburg.de
 *version: 1.0
 */

#include "SerialcomTest.h"
#include "hsm/PuckData.h"
#include <cstring>
#include <iostream>
#include <unistd.h>

#define SLEEP(a) usleep(1000000 * a)
#define DELAY 1

using namespace std;
using namespace Test;

void SerialcomTest::start() {
    SerialCom* ser = SerialCom::getInstance();
    SerialCom::Packet packet;
    memset(&packet, 0, sizeof(SerialCom::Packet));
//#define SENDER
#ifdef SENDER
    cout << "sending heartbeat" << endl;
    packet.packetType = SerialCom::HEARTBEAT;
    ser->sendPacket(&packet);
    SLEEP(DELAY);

    cout << "sending ready1" << endl;
    packet.packetType = SerialCom::READY1;
    ser->sendPacket(&packet);
    SLEEP(DELAY);

    cout << "sending ready2" << endl;
    packet.packetType = SerialCom::READY2;
    ser->sendPacket(&packet);
    SLEEP(DELAY);

    cout << "sending puck" << endl;
    packet.packetType = SerialCom::PUCK;
    packet.puck.height = 42;
    packet.puck.puckType = HOLE_WITH_METAL;
    ser->sendPacket(&packet);
    SLEEP(DELAY);

    cout << "sending estop" << endl;
    packet.packetType = SerialCom::ESTOP;
    ser->sendPacket(&packet);
#else
    // receive and print Packets content to stdout
    // ESTOP message exits the program
    bool running = true;
    cout << "waiting for messages" << endl;
    while (running) {
        ser->recvPacket(&packet);

        switch (packet.packetType) {
            case SerialCom::HEARTBEAT:
                cout << "received heartbeat" << endl;
                break;
            case SerialCom::READY1:
                cout << "received ready1" << endl;
                break;
            case SerialCom::READY2:
                cout << "received ready2" << endl;
                break;
            case SerialCom::PUCK:
                cout << "received puck" << endl;
                cout << "Puck height: " << packet.puck.height << endl;
                switch (packet.puck.puckType) {
                    case PuckType::FLAT:
                        cout << "Puck type: flat" << endl;
                        break;
                    case PuckType::HOLE_WITHOUT_METAL:
                        cout << "Puck type: hole wthout metal" << endl;
                        break;
                    case PuckType::HOLE_WITH_METAL:
                        cout << "Puck type: hole with metal" << endl;
                        break;
                    case PuckType::UPSIDEDOWN:
                        cout << "Puck type: upside down" << endl;
                        break;
                    case PuckType::UNDEFINED:
                        cout << "Puck type: undefined" << endl;
                        break;
                }
                break;
            case SerialCom::ESTOP_PRESSED:
                cout << "received ESTOP" << endl;
                running = false;
                break;
            case SerialCom::RAMP_1_FULL:
                cout << "received RAMP_1_FULL" << endl;
                break;
            case SerialCom::RAMP_2_FULL:
                cout << "received RAMP_2_FULL" << endl;
                break;
            case SerialCom::TRANSFER_FINISHED:
                cout << "received TRANSFER_FINISHED" << endl;
                break;
            case SerialCom::METAL_DETECTED:
                cout << "received METAL_DETECTED" << endl;
                break;
        }
    }
#endif
}

SerialcomTest::SerialcomTest() {}
SerialcomTest::~SerialcomTest() {}

SerialcomTest* SerialcomTest::getInstance() {
    static SerialcomTest instance_;
    return &instance_;
}
