/*
 * SerialListener.cpp
 *
 *  Created on: 23.05.2016
 *      Author: abp842
 */

#include "SerialListener.h"
#include "Sensors.h"
#include "hal/portmap.h"
#include "hsm/PuckData.h"
#include "hsm/PuckStateMachine.h"
#include "hsm/SystemStateMachine.h"
#include <cstdlib>

SerialListener::SerialListener() {
    if ((pulseConnection = ConnectAttach(0, 0, HAL::Sensors::getInstance()->get_isrChannel(), 0, 0)) == -1) {
        exit(-1);
    }
}

SerialListener::~SerialListener() {}

SerialListener* SerialListener::getInstance() {
    static SerialListener instance_;

    return &instance_;
}

void SerialListener::execute(void* in) {

    while (1) {
        getNextMessage();
    }
}

void SerialListener::shutdown() {}

void SerialListener::getNextMessage() {
    SerialCom* serial = SerialCom::getInstance();
    SerialCom::Packet packet;

    if (serial->recvPacket(&packet) == -1) {
        cout << "Error receiving Packet" << endl;
        return;
    }

    if (packet.packetType == SerialCom::PUCK) {
        PuckData::pucks.push_back(new PuckStateMachine(new PuckData(
            SystemStateMachine::incrementIdCounter(), packet.puck.puckType, packet.puck.height)));

#ifdef SYSTEM1
    } else if (packet.packetType == SerialCom::SYNCHRONIZE_EXPECTED_PUCK) {
        cout << timestamp << "   received SYNCHRONIZE_EXPECTED_PUCK PuckType: " << packet.puck.puckType
             << endl;
        PuckData::nextExpectedPuck = packet.puck.puckType;
        PuckData::systemData.ready2 = true;
#endif

    } else if (packet.packetType == SerialCom::HEARTBEAT) {
        SerialCom::getInstance()->sendMessage(SerialCom::HEARTBEAT, 0);
    } else {
        cout << timestamp << "   received " << packet.packetType << endl;
    }

    if (-1 == MsgSendPulse(pulseConnection, SIGEV_PULSE_PRIO_INHERIT, SERIAL, packet.packetType)) {
        perror("SerialCom MsgSendPulse");
    }
}
