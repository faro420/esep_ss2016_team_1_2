// Created by dimhof on 11.04.2016.
//

#include "Logger.h"
#include <iomanip>
#include <sys/time.h>

using namespace std;

ostream& Logger::timestamp(ostream& os) {
    timeval curTime;
    gettimeofday(&curTime, NULL);
    time_t now = curTime.tv_sec;
    struct tm* lt = localtime(&now);

    os  <<"[ "
        << setfill('0') << setw(2) << lt->tm_hour << ":"
        << setfill('0') << setw(2) << lt->tm_min  << ":"
        << setfill('0') << setw(2) << lt->tm_sec  << "."
        << setfill('0') << setw(3) << curTime.tv_usec/1000
        << " ] ";

    return os;
}

ostream& Logger::operator<<(ostream& os, const SerialCom::PacketType enm) {
    switch (enm) {
        case SerialCom::SYNCHRONIZE_EXPECTED_PUCK:
            os << "SYNCHRONIZE_EXPECTED_PUCK";
            break;
        case SerialCom::HEARTBEAT:
            os << "HEARTBEAT";
            break;
        case SerialCom::ESTOP_PRESSED:
            os << "ESTOP_PRESSED";
            break;
        case SerialCom::ESTOP_RELEASED:
            os << "ESTOP_RELEASED";
            break;
        case SerialCom::READY1:
            os << "READY1";
            break;
        case SerialCom::READY2:
            os << "READY2";
            break;
        case SerialCom::RAMP_1_FULL:
            os << "RAMP_1_FULL";
            break;
        case SerialCom::RAMP_2_FULL:
            os << "RAMP_2_FULL";
            break;
        case SerialCom::PUCK:
            os << "PUCK";
            break;
        case SerialCom::TRANSFER_FINISHED:
            os << "TRANSFER_FINISHED";
            break;
        case SerialCom::METAL_DETECTED:
            os << "METAL_DETECTED";
            break;
        case SerialCom::RESET:
            os << "RESET";
            break;
        case SerialCom::ROLLBACK_EXPECTED_PUCK:
            os << "ROLLBACK_EXPECTED_PUCK";
            break;
    }
    return os;
}

ostream& Logger::operator<<(ostream& os, const HAL::Actuators::SpeedLevel enm) {
    switch (enm) {
        case HAL::Actuators::NONE:
            os << "NONE";
            break;
        case HAL::Actuators::FAST:
            os << "FAST";
            break;
        case HAL::Actuators::SLOW:
            os << "SLOW";
            break;
    }
    return os;
}

ostream& Logger::operator<<(ostream& os, const HAL::Actuators::Direction enm) {
    switch (enm) {
        case HAL::Actuators::FORWARDS:
            os << "FORWARDS";
            break;
        case HAL::Actuators::BACKWARDS:
            os << "BACKWARDS";
            break;
    }
    return os;
}

ostream& Logger::operator<<(ostream& os, const HAL::Actuators::LEDColor enm) {
    switch (enm) {
        case HAL::Actuators::GREEN:
            os << "GREEN";
            break;
        case HAL::Actuators::AMBER:
            os << "AMBER";
            break;
        case HAL::Actuators::RED:
            os << "RED";
            break;
    }
    return os;
}

ostream& Logger::operator<<(ostream& os, const HAL::Actuators::LEDButton enm) {
    switch (enm) {
        case HAL::Actuators::START:
            os << "START";
            break;
        case HAL::Actuators::RESET:
            os << "RESET";
            break;
        case HAL::Actuators::Q1:
            os << "Q1";
            break;
        case HAL::Actuators::Q2:
            os << "Q2";
            break;
    }
    return os;
}

ostream& Logger::operator<<(ostream& os, const HAL::Sensors::Source enm) {
    switch (enm) {
        case HAL::Sensors::error:
            os << "error";
            break;
        case HAL::Sensors::lightbarrier_entry:
            os << "lightbarrier_entry";
            break;
        case HAL::Sensors::lightbarrier_height_measurement:
            os << "lightbarrier_height_measurement";
            break;
        case HAL::Sensors::lightbarrier_switch:
            os << "lightbarrier_switch";
            break;
        case HAL::Sensors::metall_detector:
            os << "metall_detector";
            break;
        case HAL::Sensors::lightbarrier_ramp:
            os << "lightbarrier_ramp";
            break;
        case HAL::Sensors::lightbarrier_exit:
            os << "lightbarrier_exit";
            break;
        case HAL::Sensors::button_start:
            os << "button_start";
            break;
        case HAL::Sensors::button_stop:
            os << "button_stop";
            break;
        case HAL::Sensors::button_reset:
            os << "button_reset";
            break;
        case HAL::Sensors::button_e_stop:
            os << "button_e_stop";
            break;
    }
    return os;
}

ostream& Logger::operator<<(ostream& os, const PuckType enm) {
    switch (enm) {
        case UNDEFINED:
            os << "UNDEFINED";
            break;
        case FLAT:
            os << "FLAT";
            break;
        case HOLE_WITHOUT_METAL:
            os << "HOLE_WITHOUT_METAL";
            break;
        case HOLE_WITH_METAL:
            os << "HOLE_WITH_METAL";
            break;
        case UPSIDEDOWN:
            os << "UPSIDEDOWN";
            break;
    }
    return os;
}

ostream& Logger::operator<<(ostream& os, const States enm) {
    switch (enm) {
        case NO_STATE:
            os << "NO_STATE";
            break;
        case HEIGHT_MEASUREMENT:
            os << "HEIGHT_MEASUREMENT";
            break;
        case SWITCH:
            os << "SWITCH";
            break;
        case EXIT:
            os << "EXIT";
            break;
        case TO_BE_DELETED:
            os << "TO_BE_DELETED";
            break;
    }
    return os;
}

ostream& Logger::operator<<(ostream& os, const EVENTS enm) {
    switch (enm) {
        case GO_TO_IDLE:
            os << "GO_TO_IDLE";
            break;
        case LB_ENTRY_1:
            os << "LB_ENTRY_1";
            break;
        case LB_ENTRY_2:
            os << "LB_ENTRY_2";
            break;
        case LB_HEIGHT_1:
            os << "LB_HEIGHT_1";
            break;
        case LB_HEIGHT_2:
            os << "LB_HEIGHT_2";
            break;
        case LB_SWITCH_1:
            os << "LB_SWITCH_1";
            break;
        case LB_SWITCH_2:
            os << "LB_SWITCH_2";
            break;
        case LB_RAMP_1:
            os << "LB_RAMP_1";
            break;
        case LB_RAMP_2:
            os << "LB_RAMP_2";
            break;
        case LB_EXIT_1:
            os << "LB_EXIT_1";
            break;
        case LB_EXIT_2:
            os << "LB_EXIT_2";
            break;
        case HEIGHT:
            os << "HEIGHT";
            break;
        case TRANSFER_PUCK:
            os << "TRANSFER_PUCK";
            break;
        case TRANSFER_FINISHED:
            os << "TRANSFER_FINISHED";
            break;
        case SEND_READY_1:
            os << "SEND_READY_1";
            break;
        case SEND_READY_2:
            os << "SEND_READY_2";
            break;
        case IS_METAL:
            os << "IS_METAL";
            break;
        case PUCK_EXIT:
            os << "PUCK_EXIT";
            break;
        case RAMP_1_FULL:
            os << "RAMP_1_FULL";
            break;
        case RAMP_1_ACCEPTING:
            os << "RAMP_1_ACCEPTING";
            break;
        case RAMP_2_FULL:
            os << "RAMP_2_FULL";
            break;
        case RAMP_2_ACCEPTING:
            os << "RAMP_2_ACCEPTING";
            break;
        case TIMEOUT:
            os << "TIMEOUT";
            break;
        case UPDATE_TIMER:
            os << "UPDATE_TIMER";
            break;
        case ERROR:
            os << "ERROR";
            break;
        case ERROR_2:
            os << "ERROR_2";
            break;
        case OK:
            os << "OK";
            break;
        case OK_2:
            os << "OK_2";
            break;
        case SWITCH_TIMEOUT:
            os << "SWITCH_TIMEOUT";
            break;
        case RESET_BUTTON:
            os << "RESET_BUTTON";
            break;
        case ERROR_GONE:
            os << "ERROR_GONE";
            break;
        case NEVENTS:
            os << "NEVENTS";
            break;
    }
    return os;
}
