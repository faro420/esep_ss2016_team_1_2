//
// Created by dimhof on 11.04.2016.
//

#ifndef ESEP_LOGGER_LOGGER_HPP
#define ESEP_LOGGER_LOGGER_HPP

#include "SerialCom.h"
#include "hal/Actuators.h"
#include "hal/Sensors.h"
#include "hsm/Dispatcher.h"
#include "hsm/PuckData.h"
#include <iostream>

using namespace std;

namespace Logger {

    ostream& timestamp(ostream& os);

    ostream& operator<<(ostream& os, const SerialCom::PacketType enm);

    ostream& operator<<(ostream& os, const HAL::Actuators::SpeedLevel enm);

    ostream& operator<<(ostream& os, const HAL::Actuators::Direction enm);

    ostream& operator<<(ostream& os, const HAL::Actuators::LEDColor enm);

    ostream& operator<<(ostream& os, const HAL::Actuators::LEDButton enm);

    ostream& operator<<(ostream& os, const HAL::Sensors::Source enm);

    ostream& operator<<(ostream& os, const PuckType enm);

    ostream& operator<<(ostream& os, const EVENTS enm);

    ostream& operator<<(ostream& os, const States enm);
}

#endif // ESEP_LOGGER_LOGGER_HPP
