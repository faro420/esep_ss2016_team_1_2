/**
 *	@file SerialCom.h
 *  @date 11.04.2016
 *  @version 1.0
 *  @author Darryl Imhof
 */

#ifndef SERIALCOM_SERIALCOM_H
#define SERIALCOM_SERIALCOM_H

#include "hsm/PuckData.h"
#include "timer/Timer.h"
#include <stdint.h>
#include <termios.h>

#define STARTBYTE 0xaa
#define BAUDRATE B19200

class SerialCom {
public:
    //! Type of Packet
    enum PacketType {
        HEARTBEAT,
        ESTOP_PRESSED,
        ESTOP_RELEASED,
        READY1,
        READY2,
        RAMP_1_FULL,
        RAMP_2_FULL,
        PUCK,
        TRANSFER_FINISHED,
        METAL_DETECTED,
        RESET,
        ROLLBACK_EXPECTED_PUCK,
        SYNCHRONIZE_EXPECTED_PUCK
    };

    typedef struct {
        PuckType puckType;
        uint16_t height;
    } Puck;

    //! Contains the data transmitted over the serial interface
    typedef struct {
        PacketType packetType;
        Puck puck;
    } Packet;

    static SerialCom* getInstance();

    /** Sends a Packet over the serial interface
     * @param[in] packet to sent has to be initialized before passing as parameter
     * @return returns 0 on success
     */
    int sendPacket(Packet* packet);

    /** Receives a Packet over the serial interface (blocking)
     * @param[out] packet Pointer to location where data will be returned
     * @return returns 0 on success
     */
    int recvPacket(Packet* packet);

    void sendMessage(PacketType type, PuckData* data);

private:
    static SerialCom* instance_;

    SerialCom();

    SerialCom(const SerialCom& other);

    SerialCom& operator=(const SerialCom& other);

    ~SerialCom();

    int fd;
    struct termios oldtio, newtio;
    static uint8_t startbyte;
    timer_t heartbeat;
};

//! Contains information about a Puck transmitted over the serial interface

#endif // SERIALCOM_SERIALCOM_H
