/*
 * SerialListener.h
 *
 *  Created on: 23.05.2016
 *      Author: abp842
 */

#ifndef SERIALLISTENER_H_
#define SERIALLISTENER_H_

#include "HAWThread.h"
#include "SerialCom.h"

using namespace thread;

class SerialListener : public HAWThread {
public:
    virtual void execute(void*);
    virtual void shutdown();

    //! Get instance of SerialListener class
    static SerialListener* getInstance();

private:
    SerialListener();
    virtual ~SerialListener();

    void getNextMessage();
    int pulseConnection;
};

#endif /* SERIALLISTENER_H_ */
