/**
 *  @file Actuators.h
 *  @date 20.04.2016
 *  @version 1.0
 *  @author Stefan Sylvius Wagner
 */

#ifndef ACTUATORS_H_
#define ACTUATORS_H_

#include "../HAWThread.h"
#include "../Lock.h"

using namespace thread;

/**
* The HAL wraps all direct access to the hardware this includes actuators, sensors and HID.
*/
namespace HAL {
    /**
    *  This is where all actuators functionality is wrapped. This is part of the HAL.
    *  The motor, traffic light, switch and button LED's can be controlled.
    *
    */
    class Actuators : public HAWThread {

    public:
        //! Speed levels for the conveyor belt motor.
        enum SpeedLevel { NONE, FAST, SLOW };
        //! Speed levels for the traffic light.
        enum BlinkSpeed { BLINK_NONE, BLINK_FAST, BLINK_SLOW };
        //! Direction of the conveyor belt motor.
        enum Direction { FORWARDS, BACKWARDS };
        //! LED color of the traffic light.
        enum LEDColor { GREEN, AMBER, RED }; //! LEDs of the buttons.
        enum LEDButton { START, RESET, Q1, Q2 };

        /**
        *  Starts conveyor belt motor.
        *  @param[in] Direction Direction of the motor.
        *  @param[in] SpeedLevel Speed of the motor.
        */
        void motorStart(Direction, SpeedLevel);
        //! Stops conveyor belt motor.
        void motorStop();
        //! Opens the switch.
        void switchOpen();
        //! Closes the switch.
        void switchClose();
        //! Checks if switch is already open.
        bool switchIsOpen();
        //! Checks if motor is running.
        SpeedLevel motorGetSpeedLevel();
        /** Turns the traffic light on with a specified color.
        * @param[in] LEDColor Color of the led to be turned on.
        */
        void trafficLightOn(LEDColor);
        /** Set the interval in which the led will blink.
        * @param[in] LEDColor Color of the led which will blink.
        * @param[in] BlinkSpeed The speed level in which the led will blink.
        */
        void setTrafficLightBlinkSpeed(LEDColor, BlinkSpeed);
        /** Turns the traffic light off at a specified color.
        * @param[in] LEDColor Color of the led to be turned off.
        */
        void trafficLightOff(LEDColor);
        /** Turns the button light on.
        * @param[in] LEDButton Button, which LED will be turned on.
        */
        void buttonLightOn(LEDButton);
        /** Turns the button light off.
        * @param[in] LEDButton Button, which LED will be turned off.
        */
        void buttonLightOff(LEDButton);
        /** Gets the current state of the estop switch.
        *
        */
        bool getEStopState();
        bool isInInitState();
        //! Get instance of Actuators object Singleton.
        static Actuators* getInstance();

        //! Inherited by HAWThread
        virtual void execute(void*);
        //! Inherited by HAWThread
        virtual void shutdown();

        bool isSlow;

    private:
        static Actuators* instance_;

        Actuators();
        Actuators(const Actuators& other);
        Actuators& operator=(const Actuators& other);
        ~Actuators();

        static pthread_mutex_t mtx_;
        SpeedLevel motorSpeedLevel_;
        bool open_;
        BlinkSpeed green;
        BlinkSpeed amber;
        BlinkSpeed red;
    };
}

#endif /* ACTUATORS_H_ */
