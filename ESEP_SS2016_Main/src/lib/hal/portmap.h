/**
 * @file portmap.h
 * This is where all ports are mapped to control the hardware
 *
 * @date 05.04.2016
 * @author abp842
 *
 */

#ifndef PORTMAP_H_
#define PORTMAP_H_

#include <sys/neutrino.h>

/** @name Base Addresses for I/O Ports */
//@{
#define PORT_A_BASE (0x300)
#define PORT_B_BASE (0x301)
#define PORT_C_BASE (0x302)
#define HEIGHT_MEASURE_BASE_ADDRESS (0x320)
//@}

/** @name Specific functions for Port A (Output) */
//@{
#define MOTOR_FORWARDS_BIT 					(0b00000001)
#define MOTOR_BACKWARDS_BIT 				(0b00000010)
#define MOTOR_SLOW_BIT 						(0b00000100)
#define MOTOR_STOP_BIT 						(0b00001000)
#define SWITCH_OPEN_BIT 					(0b00010000)
#define GREEN_LED_BIT 						(0b00100000)
#define AMBER_LED_BIT 						(0b01000000)
#define RED_LED_BIT 						(0b10000000)
//@}

/** @name Specific functions for Port B (Input) */
//@{
#define LIGHTBARRIER_ENTRY_BIT 				(0b00000001)
#define LIGHTBARRIER_HEIGHT_MEASUREMENT_BIT (0b00000010)
#define HEIGHT_MEASUREMENT_BIT 				(0b00000100)
#define LIGHTBARRIER_SWITCH_BIT 			(0b00001000)
#define METALL_DETECTOR_BIT 				(0b00010000)
#define IS_SWITCH_OPEN_BIT 					(0b00100000)
#define LIGHTBARRIER_RAMP_BIT 				(0b01000000)
#define LIGHTBARRIER_EXIT_BIT 				(0b10000000)
//@{

/** @name Specific functions for Port C (Output/Input) */
//@{
#define LED_BUTTON_START_BIT 				(0b00000001)
#define LED_BUTTON_RESET_BIT 				(0b00000010)
#define LED_Q1_BIT			 				(0b00000100)
#define LED_Q2_BIT 							(0b00001000)
#define BUTTON_START_BIT 					(0b00010000)
#define BUTTON_STOP_BIT 					(0b00100000)
#define BUTTON_RESET_BIT 					(0b01000000)
#define BUTTON_E_STOP_BIT 					(0b10000000)
//@}

/** @name Addresses for reading height from height sensor */
//@{
#define HEIGHT_LOW_ADDRESS_OFFSET 			(0x2)
#define HEIGHT_HIGH_ADDRESS_OFFSET 			(0x3)
#define START_CONVERSION_OFFSET 			(0x2)
#define START_CONVERSION_OPCODE 			(0x10)
#define HEIGHT_MASK 						(0x0FFF)
//@}

/** @name Interrupt register adresses */
//@{
#define IRQ_STATUS_REGISTER 				(0x30F)
#define INTERRUPT_ENABLE_REGISTER 			(0x30B)
//@}

/** @name Pulse Codes */
//@{
#define ISR 								(_PULSE_CODE_MINAVAIL)
#define SERIAL 								(ISR + 1)
#define TIMER 								(SERIAL + 1)
#define INTERN 								(SERIAL + 2)
//@}

#define SYSTEM1
#include "../lib/HWaccess.h"

#endif /* PORTMAP_H_ */
