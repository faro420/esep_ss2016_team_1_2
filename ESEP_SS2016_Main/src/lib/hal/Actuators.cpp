/**
 *
 *  @date 20.04.2016
 *  @version 1.0
 *  @author Stefan Sylvius Wagner
 */
#include "Actuators.h"
#include "../hsm/Constants.h"
#include "../hsm/SystemStateMachine.h"
#include "../timer/Timer.h"
#include "portmap.h"
#include <bitset>
#include <cstdlib>
#include <iostream>
#include <unistd.h>
using namespace std;
using namespace HAL;

pthread_mutex_t Actuators::mtx_ = PTHREAD_MUTEX_INITIALIZER;

// Motor control
void Actuators::motorStart(Direction d, SpeedLevel sl) {
    Lock lock(&mtx_);
    // Clear activated bits
    out8(PORT_A_BASE, in8(PORT_A_BASE) & ~MOTOR_STOP_BIT);
    out8(PORT_A_BASE, in8(PORT_A_BASE) & ~(MOTOR_FORWARDS_BIT | MOTOR_BACKWARDS_BIT | MOTOR_SLOW_BIT));

    if (sl == SLOW) {
        out8(PORT_A_BASE, in8(PORT_A_BASE) | MOTOR_SLOW_BIT);
        cout << "Tick Slow" << Constants::PUCK_TICK_SLOW << endl;
        Timer::getInstance()->modifyTimer(SystemStateMachine::puckTickTimer, 0, Constants::PUCK_TICK_SLOW,
                                          true);
    } else {
        Timer::getInstance()->modifyTimer(SystemStateMachine::puckTickTimer, 0, Constants::PUCK_TICK_FAST,
                                          true);
    }

    switch (d) {
        case FORWARDS:
            out8(PORT_A_BASE, in8(PORT_A_BASE) | MOTOR_FORWARDS_BIT);
            break;

        case BACKWARDS:
            out8(PORT_A_BASE, in8(PORT_A_BASE) | MOTOR_BACKWARDS_BIT);
            break;
    }

    motorSpeedLevel_ = sl;
}

// Switch control
void Actuators::switchOpen() {
    Lock lock(&mtx_);

    out8(PORT_A_BASE, in8(PORT_A_BASE) | SWITCH_OPEN_BIT);
}

void Actuators::switchClose() {
    Lock lock(&mtx_);

    out8(PORT_A_BASE, in8(PORT_A_BASE) & ~SWITCH_OPEN_BIT);
}

bool Actuators::switchIsOpen() {
    Lock lock(&mtx_);

    return in8(PORT_B_BASE & IS_SWITCH_OPEN_BIT);
}

Actuators::SpeedLevel Actuators::motorGetSpeedLevel() {
    return motorSpeedLevel_;
}

void Actuators::motorStop() {
    Lock lock(&mtx_);

    out8(PORT_A_BASE, in8(PORT_A_BASE) & ~(MOTOR_FORWARDS_BIT | MOTOR_BACKWARDS_BIT | MOTOR_SLOW_BIT));
    out8(PORT_A_BASE, in8(PORT_A_BASE) | MOTOR_STOP_BIT);

    motorSpeedLevel_ = NONE;
}

void Actuators::trafficLightOn(LEDColor led) {
    Lock lock(&mtx_);

    switch (led) {
        case GREEN:
            out8(PORT_A_BASE, in8(PORT_A_BASE) | GREEN_LED_BIT);
            break;

        case AMBER:
            out8(PORT_A_BASE, in8(PORT_A_BASE) | AMBER_LED_BIT);
            break;

        case RED:
            out8(PORT_A_BASE, in8(PORT_A_BASE) | RED_LED_BIT);
            break;
        default:
            cout << "idiot" << endl;
            break;
    }
}

void Actuators::setTrafficLightBlinkSpeed(LEDColor led, BlinkSpeed speed) {
    switch (led) {
        case GREEN:
            green = speed;
            break;

        case AMBER:
            amber = speed;
            break;

        case RED:
            red = speed;
            break;
        default:
            cout << "idiot" << endl;
            break;
    }
}

void Actuators::trafficLightOff(LEDColor led) {
    Lock lock(&mtx_);

    switch (led) {
        case GREEN:
            out8(PORT_A_BASE, in8(PORT_A_BASE) & ~GREEN_LED_BIT);
            break;

        case AMBER:
            out8(PORT_A_BASE, in8(PORT_A_BASE) & ~AMBER_LED_BIT);
            break;

        case RED:
            out8(PORT_A_BASE, in8(PORT_A_BASE) & ~RED_LED_BIT);
            break;
        default:
            cout << "idiot" << endl;
            break;
    }
}

void Actuators::buttonLightOn(LEDButton button) {
    Lock lock(&mtx_);

    switch (button) {
        case START:
            out8(PORT_C_BASE, in8(PORT_C_BASE) | LED_BUTTON_START_BIT);
            break;

        case RESET:
            out8(PORT_C_BASE, in8(PORT_C_BASE) | LED_BUTTON_RESET_BIT);
            break;

        case Q1:
            out8(PORT_C_BASE, in8(PORT_C_BASE) | LED_Q1_BIT);
            break;
        case Q2:
            out8(PORT_C_BASE, in8(PORT_C_BASE) | LED_Q2_BIT);
            break;
        default:
            cout << "idiot" << endl;
            break;
    }
}

void Actuators::buttonLightOff(LEDButton button) {
    Lock lock(&mtx_);

    switch (button) {
        case START:
            out8(PORT_C_BASE, in8(PORT_C_BASE) & ~LED_BUTTON_START_BIT);
            break;

        case RESET:
            out8(PORT_C_BASE, in8(PORT_C_BASE) & ~LED_BUTTON_RESET_BIT);
            break;

        case Q1:
            out8(PORT_C_BASE, in8(PORT_C_BASE) & ~LED_Q1_BIT);
            break;
        case Q2:
            out8(PORT_C_BASE, in8(PORT_C_BASE) & ~LED_Q2_BIT);
            break;
        default:
            cout << "idiot" << endl;
            break;
    }
}

bool Actuators::getEStopState() {
    Lock lock(&mtx_);

    return !(in8(PORT_C_BASE) & BUTTON_E_STOP_BIT);
}

bool Actuators::isInInitState() {
    Lock lock(&mtx_);

    // cout << "BITS " << std::bitset<8>(in8(PORT_B_BASE)) << endl;

    return (in8(PORT_C_BASE) & BUTTON_E_STOP_BIT) && (in8(PORT_B_BASE) == (0b11001011));
}

void Actuators::execute(void*) {
    while (true) {
        usleep(1000000);

        if (green == BLINK_FAST) {
            this->trafficLightOff(GREEN);
        }
        if (amber == BLINK_FAST) {
            this->trafficLightOff(AMBER);
        }
        if (red == BLINK_FAST) {
            this->trafficLightOff(RED);
        }

        usleep(1000000);

        if (green == BLINK_FAST) {
            this->trafficLightOn(GREEN);
        } else if (green == BLINK_SLOW) {
            this->trafficLightOff(GREEN);
        }
        if (amber == BLINK_FAST) {
            this->trafficLightOn(AMBER);
        } else if (amber == BLINK_SLOW) {
            this->trafficLightOff(AMBER);
        }
        if (red == BLINK_FAST) {
            this->trafficLightOn(RED);
        } else if (red == BLINK_SLOW) {
            this->trafficLightOff(RED);
        }

        usleep(1000000);

        if (green == BLINK_FAST) {
            this->trafficLightOff(GREEN);
        }
        if (amber == BLINK_FAST) {
            this->trafficLightOff(AMBER);
        }
        if (red == BLINK_FAST) {
            this->trafficLightOff(RED);
        }

        usleep(1000000);

        if (green != BLINK_NONE) {
            this->trafficLightOn(GREEN);
        }
        if (amber != BLINK_NONE) {
            this->trafficLightOn(AMBER);
        }
        if (red != BLINK_NONE) {
            this->trafficLightOn(RED);
        }
    }
}

void Actuators::shutdown() {}

Actuators::Actuators()
    : isSlow(false), motorSpeedLevel_(NONE), open_(false), green(BLINK_NONE), amber(BLINK_NONE),
      red(BLINK_NONE) {
    if (ThreadCtl(_NTO_TCTL_IO_PRIV, 0) == -1) {
        exit(-1);
    }
}
Actuators::~Actuators() {}

Actuators* Actuators::getInstance() {
    static Actuators instance_;

    return &instance_;
}
