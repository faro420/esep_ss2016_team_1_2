/**
 *	@file Sensors.h
 *  @date 25.04.2016
 *  @version 1.0
 *  @author Darryl Imhof
 */

#ifndef SENSORS_H_
#define SENSORS_H_

#include "portmap.h"
#include <stdint.h>

#define INT_ENABLE_B (0b11111101)
#define INT_ENABLE_C (0b11111011)
#define NIBBLE (4)
#define PORT_B_INT_MASK (0b11011111)
#define PORT_C_INT_MASK (0b11110000)

namespace HAL {
    /**
     * Registers and Handles Interrupts for Inputs on Port B and C
     *
     */
    class Sensors {
    public:
        //! Possible sources for interrupts
        enum Source {
            error,
            lightbarrier_entry,
            lightbarrier_height_measurement,
            lightbarrier_switch,
            metall_detector,
            lightbarrier_ramp,
            lightbarrier_exit,
            button_start,
            button_stop,
            button_reset,
            button_e_stop,
        };

    public:
        static int old_reg_val;
        static int isrConnection;
        //! Get instance of Sensors class
        static Sensors* getInstance();

        //! Getter for isrChannel, needed for MsgReceivePulse
        int get_isrChannel();

        /** Decodes the payload of a pulsemessage
         * @param sival_int
         * @return sensor that triggered the interrupt
         */
        Source msgDecoder(int sival_int);
        uint16_t getSensorHeight(void);
        bool isInHeightMeassurementTolerance(void);

    private:
        static Sensors* instance_;

        Sensors();
        Sensors(const Sensors& other);
        Sensors& operator=(const Sensors& other);
        ~Sensors();

        int isrId;
        int isrChannel;
        struct sigevent isrEvent;

        void registerISR(void);
        void unregisterISR(void);
    };
}

#endif // SENSORS_H_
