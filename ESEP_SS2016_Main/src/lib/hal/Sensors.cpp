/**
 *	@file Sensors.cpp
 *  @date 25.04.2016
 *  @version 1.0
 *  @author Darryl Imhof
 */

#define EXIT_FAILURE (-1)

#include <bitset>
#include <cstdlib>
#include <iostream>
#include <sys/neutrino.h>
#include <sys/siginfo.h>
#include <unistd.h>

#include "Sensors.h"

using namespace std;
using namespace HAL;

int Sensors::isrConnection = 0;
int Sensors::old_reg_val = 0;

Sensors::Sensors() {
    isrId = 0;
    isrChannel = 0;

    if (ThreadCtl(_NTO_TCTL_IO_PRIV, 0) == -1) {
        exit(EXIT_FAILURE);
    }

    if ((isrChannel = ChannelCreate(0)) == -1) {
        exit(EXIT_FAILURE);
    }

    if ((isrConnection = ConnectAttach(0, 0, isrChannel, 0, 0)) == -1) {
        exit(EXIT_FAILURE);
    }

    Sensors::old_reg_val =
        (in8(PORT_B_BASE) & PORT_B_INT_MASK) | (((in8(PORT_C_BASE) & PORT_C_INT_MASK) << NIBBLE));

    registerISR();
}

Sensors::~Sensors() {
    unregisterISR();
    if (ConnectDetach(isrConnection) == -1) {
        exit(EXIT_FAILURE);
    }

    if (ChannelDestroy(isrChannel) == -1) {
        exit(EXIT_FAILURE);
    }
}

/** Interrupt Service Routine for GPIO Port B & C
  * @param arg sigevent struct
  * @param id ISR_ID
  * @return sigevent struct containing the pulse message
  */
const struct sigevent* ISR_DIO(void* arg, int id) {
    int iir;
    struct sigevent* event = (struct sigevent*)arg;

    iir = in8(IRQ_STATUS_REGISTER) & 0b00001010; // reading IRQ_STATUS_REGISTER
    out8(IRQ_STATUS_REGISTER, 0);                // clear IRQ_STATUS_REGISTER
    if (iir & 1) {
        return NULL;
    }
    // merge PORT_B and PORT_C to 16bit variable (only 12 bit needed)
    int new_reg_val = (in8(PORT_B_BASE) & PORT_B_INT_MASK) | ((in8(PORT_C_BASE) & PORT_C_INT_MASK) << NIBBLE);
    int sival_int = new_reg_val ^ Sensors::old_reg_val;
    Sensors::old_reg_val = new_reg_val;

    if (sival_int == 0) {
        return NULL;
    }

    event->sigev_notify = SIGEV_PULSE;
    event->__sigev_un1.__sigev_coid = Sensors::isrConnection;
    event->__sigev_un2.__st.__sigev_code = ISR;

    event->sigev_value.sival_int = sival_int;

    return event;
}

/**
*	This function reads the height from the height sensor.
*	@return Returns 16 bit height value
*/
uint16_t Sensors::getSensorHeight(void) {
    uint16_t height;

    // Read low 8 bits of height
    height = in8(HEIGHT_MEASURE_BASE_ADDRESS + HEIGHT_LOW_ADDRESS_OFFSET);

    // Initiate analog to digital conversion
    out8(HEIGHT_MEASURE_BASE_ADDRESS + START_CONVERSION_OFFSET, START_CONVERSION_OPCODE);
    usleep(10);

    // Read height 8 bits of height
    height |= (uint16_t)in8(HEIGHT_MEASURE_BASE_ADDRESS + HEIGHT_HIGH_ADDRESS_OFFSET) << 8;
    height &= HEIGHT_MASK;

    return height;
}

void Sensors::registerISR(void) {
    // clear IRQ Status Register
    out8(IRQ_STATUS_REGISTER, 0);
    // set "change of state interrupt" for port B and C (low active)
    out8(INTERRUPT_ENABLE_REGISTER, INT_ENABLE_B & INT_ENABLE_C);

    SIGEV_PULSE_INIT(&isrEvent, isrConnection, SIGEV_PULSE_PRIO_INHERIT, 0, 0);
    // TODO nachlesen wofür die 11 steht und als macro auslagern ( interrupt vector number ?)
    isrId = InterruptAttach(11, ISR_DIO, &isrEvent, sizeof(isrEvent), 0);
    if (isrId == -1) {
        exit(EXIT_FAILURE);
    }
}

void Sensors::unregisterISR(void) {
    if (InterruptDetach(isrId) == -1) {
        exit(EXIT_FAILURE);
    }
    // clear "change of state interrupt" for all ports (low active)
    out8(0x30B, 0b11111111);
    // clear IRQ Status Register
    out8(0x30F, 0);
}

bool Sensors::isInHeightMeassurementTolerance(void) {
    return (in8(PORT_B_BASE) & HEIGHT_MEASUREMENT_BIT) == 0;
}

Sensors* Sensors::getInstance() {
    static Sensors instance_;

    return &instance_;
}

// TODO besseren weg finden die daten in sival_int zu kodieren
Sensors::Source Sensors::msgDecoder(int sival_int) {
    switch (sival_int) {
        case LIGHTBARRIER_ENTRY_BIT:
            return lightbarrier_entry;
        case LIGHTBARRIER_HEIGHT_MEASUREMENT_BIT:
            return lightbarrier_height_measurement;
        case LIGHTBARRIER_SWITCH_BIT:
            return lightbarrier_switch;
        case METALL_DETECTOR_BIT:
            return metall_detector;
        case LIGHTBARRIER_RAMP_BIT:
            return lightbarrier_ramp;
        case LIGHTBARRIER_EXIT_BIT:
            return lightbarrier_exit;
        // ab hier GPIO C 4-7 auf BIT 8-11 von sival_int
        case BUTTON_START_BIT << NIBBLE:
            return button_start;
        case BUTTON_STOP_BIT << NIBBLE:
            return button_stop;
        case BUTTON_RESET_BIT << NIBBLE:
            return button_reset;
        case BUTTON_E_STOP_BIT << NIBBLE:
            return button_e_stop;

        default:
            bitset<16> ib = sival_int;
            cout << "ungueltiger interrupt wert: " << ib << endl;
            return error;
    }
}

int Sensors::get_isrChannel() {
    return isrChannel;
}
