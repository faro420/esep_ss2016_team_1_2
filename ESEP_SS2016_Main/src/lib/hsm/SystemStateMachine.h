/*
 * SystemStateMachine.h
 *
 *  Created on: 13.05.2016
 *      Author: abp842
 */

#ifndef SYSTEMSTATEMACHINE_H_
#define SYSTEMSTATEMACHINE_H_

#include "../SerialCom.h"
#include "../hal/Actuators.h"
#include "../timer/Timer.h"
#include "Dispatcher.h"
#include "Transitions.h"

#include "ErrorStateMachine.h"
#include "InitStateMachine.h"
#include "PuckStateMachine.h"
#include <fstream>
#include <iostream>
#include <queue>
using namespace std;
using namespace HAL;

class SystemStateMachine {
private:
    static int puckIdCounter;

    struct State : public Transitions {

        virtual void LB_Entry_1(){};
        virtual void LB_Entry_2(){};
        virtual void LB_Height_1(){};
        virtual void LB_Height_2(){};
        virtual void LB_Switch_1(){};
        virtual void LB_Switch_2(){};
        virtual void LB_Ramp_1(){};
        virtual void LB_Ramp_2(){};
        virtual void LB_Exit_1(){};
        virtual void LB_Exit_2(){};
        virtual void transferPuck(){};
        virtual void transferFinished(){};
        virtual void sendReady1(){};
        virtual void onHeight(){};
        virtual void sendReady2(){};
        virtual void isMetal(){};
        virtual void puckExit(){};
        virtual void switchTimeout(){};
        virtual void ramp1Full(){};
        virtual void ramp1Accepting(){};
        virtual void ramp2Full(){};
        virtual void ramp2Accepting(){};
        virtual void timeout(){};
        virtual void updateTimer(){};
        virtual void error(){};
        virtual void error2(){};
        virtual void ok(){};
        virtual void ok2(){};
        virtual void resetButton(){};
        virtual void errorGone(){};
        virtual void goToIdle(){};
        virtual void estop(){};
    } * state;

    struct Init : public State {
        int state;
        InitStateMachine* psm_initialize;
        PuckData* puck;
        uint16_t ticksFast;
        uint16_t ticksSlow;

        virtual void LB_Entry_1() {
            if (state == 3) {
                Actuators::getInstance()->motorStart(Actuators::FORWARDS, Actuators::SLOW);
                Dispatcher::getInstance()->addListeners(this, LB_HEIGHT_1);
                Dispatcher::getInstance()->addListeners(this, UPDATE_TIMER);
                Dispatcher::getInstance()->remListeners(this, LB_ENTRY_1);
            } else if (state == 4) {
                Dispatcher::getInstance()->remListeners(this, LB_ENTRY_1);
                ticksFast = puck->getTicks();
                puck->resetTicks();
                Actuators::getInstance()->motorStop();

                Constants::PUCK_TICK_SLOW =
                    (int)((((double)ticksSlow) / ((double)ticksFast)) * ((double)Constants::PUCK_TICK_FAST));

                delete psm_initialize;

                std::fstream fs;
                fs.open("calibration.cfg", std::fstream::out);
                fs << Constants::FLAT_HEIGHT_MIN << "\n"
                   << Constants::FLAT_HEIGHT_MAX << "\n"
                   << Constants::HOLE_HEIGHT_MIN << "\n"
                   << Constants::HOLE_HEIGHT_MAX << "\n"
                   << Constants::TIMER_HEIGHT_MIN << "\n"
                   << Constants::TIMER_SWITCH_MIN << "\n"
                   << Constants::TIMER_EXIT_MIN << "\n"
                   << Constants::TIMER_HEIGHT_MAX << "\n"
                   << Constants::TIMER_SWITCH_MAX << "\n"
                   << Constants::TIMER_EXIT_MAX << "\n"
                   << Constants::PUCK_TICK_SLOW << "\n";
                fs.close();

                SerialCom::getInstance()->sendMessage(SerialCom::HEARTBEAT, 0);
                new (this) Idle;
            }
        }

        virtual void updateTimer() {
            puck->incrementTicks();
        }

        virtual void LB_Height_1() {
            if (state == 3) {
                Actuators::getInstance()->motorStart(Actuators::BACKWARDS, Actuators::FAST);
                ticksSlow = puck->getTicks();
                puck->resetTicks();
                Dispatcher::getInstance()->remListeners(this, LB_HEIGHT_1);
                Dispatcher::getInstance()->addListeners(this, LB_ENTRY_1);
                state = 4;
            }
        }

        virtual void puckExit() {
            switch (state) {
                case 1: {
                    cout << "lege puck mit loch rechts am band ein" << endl;
                    state = 2;

                    PuckData* pdata = new PuckData(state);

                    psm_initialize = new (psm_initialize) InitStateMachine(pdata);
                    break;
                }
                case 2: {
                    cout << "lege puck links am band ein" << endl;
                    state = 3;

                    Dispatcher::getInstance()->addListeners(this, LB_ENTRY_1);

                    break;
                }
            }
        }

        Init() {
            std::ifstream fs;
            if (std::ifstream("calibration.cfg")) {
                cout << "reading calibration file:" << endl;

                std::fstream fs;
                fs.open("calibration.cfg", std::fstream::in);

                fs >> Constants::FLAT_HEIGHT_MIN >> Constants::FLAT_HEIGHT_MAX >>
                    Constants::HOLE_HEIGHT_MIN >> Constants::HOLE_HEIGHT_MAX >> Constants::TIMER_HEIGHT_MIN >>
                    Constants::TIMER_SWITCH_MIN >> Constants::TIMER_EXIT_MIN >> Constants::TIMER_HEIGHT_MAX >>
                    Constants::TIMER_SWITCH_MAX >> Constants::TIMER_EXIT_MAX >> Constants::PUCK_TICK_SLOW;

                fs.close();


                cout    << "\t" << "flat_height_min: "  << Constants::FLAT_HEIGHT_MIN   << endl
                        << "\t" << "flat_height_max: "  << Constants::FLAT_HEIGHT_MAX   << endl
                        << "\t" << "hole_height_min: "  << Constants::HOLE_HEIGHT_MIN   << endl
                        << "\t" << "hole_height_max: "  << Constants::HOLE_HEIGHT_MAX   << endl
                        << "\t" << "timer_height_min: " << Constants::TIMER_HEIGHT_MIN  << endl
                        << "\t" << "timer_switch_min: " << Constants::TIMER_SWITCH_MIN  << endl
                        << "\t" << "timer_exit_min: "   << Constants::TIMER_EXIT_MIN    << endl
                        << "\t" << "timer_height_max: " << Constants::TIMER_HEIGHT_MAX  << endl
                        << "\t" << "timer_switch_max: " << Constants::TIMER_SWITCH_MAX  << endl
                        << "\t" << "timer_exit_max: "   << Constants::TIMER_EXIT_MAX    << endl
                        << "\t" << "puck_tick_slow: "   << Constants::PUCK_TICK_SLOW    << endl;

                SerialCom::getInstance()->sendMessage(SerialCom::HEARTBEAT, 0);
                new (this) Idle;
                return;
            } else {
                cout << "calibration file not found" << endl << "starting calibration" << endl;
                puck = new PuckData(0);
                ticksFast = 0;
                ticksSlow = 0;
            }

            Dispatcher::getInstance()->addListeners(this, PUCK_EXIT);
            state = 1;

            cout << "lege flachen puck links am band ein" << endl;
            PuckData* pdata = new PuckData(state);
            psm_initialize = new InitStateMachine(pdata);
        }
    };

    struct Idle : public State {
        virtual void LB_Ramp_2() {
#ifdef SYSTEM1
            if (PuckData::systemData.ramp1Full) {
#endif
#ifdef SYSTEM2
                if (PuckData::systemData.ramp2Full) {
#endif
                    Actuators::getInstance()->setTrafficLightBlinkSpeed(Actuators::AMBER,
                                                                        Actuators::BLINK_NONE);
                    Actuators::getInstance()->trafficLightOff(Actuators::AMBER);
#ifdef SYSTEM1
                    PuckData::systemData.ramp1Full = false;
                    SerialCom::getInstance()->sendMessage(SerialCom::RAMP_1_FULL, NULL);
#endif
#ifdef SYSTEM2
                    PuckData::systemData.ramp2Full = false;
                    SerialCom::getInstance()->sendMessage(SerialCom::RAMP_2_FULL, NULL);
#endif
                    if (Dispatcher::error_flag) {
                        Dispatcher::getInstance()->callListeners(ERROR_GONE);
                    }
                }
            }

#ifdef SYSTEM1

            virtual void sendReady2() {
                PuckData::systemData.ready2 = true;
            }

            virtual void LB_Entry_1() {
                cout << "Create PuckData" << endl;
                PuckData* pdata = new PuckData(puckIdCounter++);
                cout << "Create PuckStateMachine" << endl;
                PuckStateMachine* psm = new PuckStateMachine(pdata);
                cout << "Push Pucks" << endl;
                PuckData::pucks.push_back(psm);
                cout << "new puck in ssm idle new size:" << PuckData::pucks.size() << endl;
                new (this) AtLeast1Puck;
            }

            void timeout() {
                if (TimeKeeper::getInstance()->checkTimeout()) {
                    Dispatcher::getInstance()->callListeners(ERROR);
                    cout << "CHECK TIMER IDLE ERROR " << endl;
                }
            }

            Idle() {
                Actuators::getInstance()->trafficLightOn(Actuators::GREEN);
                Actuators::getInstance()->motorStop();
                Dispatcher* disp = Dispatcher::getInstance();
                disp->addListeners(this, LB_ENTRY_1);
                disp->addListeners(this, SEND_READY_2);
                disp->addListeners(this, TIMEOUT);
                disp->addListeners(this, LB_RAMP_2);
            }
#endif

#ifdef SYSTEM2

            virtual void sendReady1() {
                new (this) Accepting;
            }

            virtual void timeout() {
                cout << "Timeout IDLE System 2" << endl;
                //  Loggr::getInstance()->log(Logger::Info, "timeout3");
                if (TimeKeeper::getInstance()->checkTimeout()) {
                    Dispatcher::getInstance()->callListeners(ERROR);
                }
            }

            Idle() {
                cout << "state: idle" << endl;
                Actuators::getInstance()->trafficLightOn(Actuators::GREEN);
                Actuators::getInstance()->motorStop();
                Dispatcher* disp = Dispatcher::getInstance();
                disp->addListeners(this, SEND_READY_1);
                disp->addListeners(this, LB_RAMP_2);
                disp->addListeners(this, TIMEOUT);
            }
#endif
        };

#ifdef SYSTEM1
        struct AtLeast1Puck : public State {
            // TODO ordentlich machen, error zustand

            virtual void sendReady2() {
                PuckData::systemData.ready2 = true;
            }

            virtual void LB_Ramp_2() {
                if (PuckData::systemData.ramp1Full) {
                    Actuators::getInstance()->setTrafficLightBlinkSpeed(Actuators::AMBER,
                                                                        Actuators::BLINK_NONE);
                    Actuators::getInstance()->trafficLightOff(Actuators::AMBER);

                    PuckData::systemData.ramp1Full = false;
                    cout << "irgendwas2" << endl;
                    SerialCom::getInstance()->sendMessage(SerialCom::RAMP_1_FULL, NULL);
                    if (Dispatcher::error_flag) {
                        Dispatcher::getInstance()->callListeners(ERROR_GONE);
                    }
                }
            }

            virtual void puckExit() {
                for (deque<PuckStateMachine*>::iterator it = PuckData::pucks.begin();
                     it != PuckData::pucks.end(); ++it) {
                    if ((*it)->state->currentPuck->getState() == TO_BE_DELETED) {
                        delete *it;
                        PuckData::pucks.erase(it);
                        break;
                    }
                }

                if (PuckData::pucks.empty()) {
                    new (this) Idle;
                }
            }
            virtual void timeout() {
                //  Logger::getInstance()->log(Logger::Info, "timeout3");
                if (TimeKeeper::getInstance()->checkTimeout()) {
                    Dispatcher::getInstance()->callListeners(ERROR);
                    cout << "CHECK TIMER ATLEAST1PUCK ERROR " << endl;
                }
            }

            virtual void LB_Entry_1() {
                PuckData* pdata = new PuckData(puckIdCounter++);
                PuckStateMachine* psm = new PuckStateMachine(pdata);
                PuckData::pucks.push_back(psm);
                cout << "new puck in ssm atleast1puck new size:" << PuckData::pucks.size() << endl;
            }

            virtual void goToIdle() {
                new (this) Idle;
            }

            AtLeast1Puck() {
                Actuators::getInstance()->motorStart(Actuators::FORWARDS, Actuators::FAST);
                Dispatcher* disp = Dispatcher::getInstance();
                disp->remListeners(this, ERROR);
                disp->addListeners(this, SEND_READY_2);
                disp->addListeners(this, PUCK_EXIT);
                disp->addListeners(this, GO_TO_IDLE);
            }
        };
#endif

#ifdef SYSTEM2

        struct Only1Puck : public State {

            virtual void LB_Ramp_2() {
                if (PuckData::systemData.ramp2Full) {
                    Actuators::getInstance()->setTrafficLightBlinkSpeed(Actuators::AMBER,
                                                                        Actuators::BLINK_NONE);
                    Actuators::getInstance()->trafficLightOff(Actuators::AMBER);
                    PuckData::systemData.ramp2Full = false;
                    SerialCom::getInstance()->sendMessage(SerialCom::RAMP_2_FULL, NULL);
                    if (Dispatcher::error_flag) {
                        Dispatcher::getInstance()->callListeners(ERROR_GONE);
                    }
                }
            }

            virtual void goToIdle() {
                new (this) Idle;
            }

            virtual void sendReady1() {
                PuckData::systemData.ready1 = true;
            }

            virtual void puckExit() {
                if (!PuckData::pucks.empty()) {
                    delete PuckData::pucks.front();
                    PuckData::pucks.pop_front();
                }
                cout << "ready ssm " << PuckData::systemData.ready1 << endl;
                if (PuckData::systemData.ready1) {
                    new (this) Accepting;
                } else {
                    new (this) Idle;
                }
            }

            virtual void timeout() {

                //  Logger::getInstance()->log(Logger::Info, "timeout3");
                if (TimeKeeper::getInstance()->checkTimeout()) {
                    Dispatcher::getInstance()->callListeners(ERROR);
                }
            }

            Only1Puck() {
                cout << "state: only1puck" << endl;

                Dispatcher* disp = Dispatcher::getInstance();
                disp->remListeners(this, TRANSFER_FINISHED);
                disp->remListeners(this, ERROR_2);
                disp->addListeners(this, PUCK_EXIT);
            }
        };

        struct Accepting : public State {

            virtual void LB_Entry_1() {
                // TODO createPuck, pushPuck
                new (this) Only1Puck;
            }

            virtual void goToIdle() {
                new (this) Idle;
            }

            Accepting() {
                cout << "state: accepting" << endl;
                Actuators::getInstance()->motorStart(Actuators::FORWARDS, Actuators::FAST);
                Dispatcher* disp = Dispatcher::getInstance();
                disp->addListeners(this, LB_ENTRY_1);
                disp->addListeners(this, GO_TO_IDLE);
            }
        };
#endif

#ifdef SYSTEM1

        struct Error_Handling : public State {

            virtual void error() {}

            Error_Handling() {
                Dispatcher* disp = Dispatcher::getInstance();
                disp->remListeners(this, OK);
                disp->addListeners(this, ERROR);
            }
        };
#endif

#ifdef SYSTEM2

        // TODO entfernen ? wird das hier �berhaubt genutzt
        struct Error_Handling2 : public State {

            virtual void error2() {}

            Error_Handling2() {
                Dispatcher* disp = Dispatcher::getInstance();
                disp->remListeners(this, OK_2);
                disp->addListeners(this, ERROR_2);
            }
        };
#endif

    public:
        static timer_t puckTickTimer;
        static int incrementIdCounter();

        SystemStateMachine();

        ~SystemStateMachine();
    };

#endif /* SYSTEMSTATEMACHINE_H_ */
