/*
 * ErrorStateMachine.h
 *
 *  Created on: 02.06.2016
 *      Author: abj718
 */

#ifndef ERRORSTATEMACHINE_H_
#define ERRORSTATEMACHINE_H_

#include "../SerialCom.h"
#include "../hal/Actuators.h"
#include "../hal/portmap.h"
#include "../hsm/Dispatcher.h"
#include "../timer/Timer.h"
#include "PuckData.h"
#include "PuckStateMachine.h"
#include "Transitions.h"

using namespace HAL;

class ErrorStateMachine {
public:
    ErrorStateMachine();
    virtual ~ErrorStateMachine();

private:
    struct State : public Transitions {
        Actuators::SpeedLevel motorSpeedLevel;

        virtual void LB_Entry_1(){};
        virtual void LB_Entry_2(){};
        virtual void LB_Height_1(){};
        virtual void LB_Height_2(){};
        virtual void LB_Switch_1(){};
        virtual void LB_Switch_2(){};
        virtual void LB_Ramp_1(){};
        virtual void LB_Ramp_2(){};
        virtual void LB_Exit_1(){};
        virtual void LB_Exit_2(){};
        virtual void transferPuck(){};
        virtual void transferFinished(){};
        virtual void sendReady1(){};
        virtual void onHeight(){};
        virtual void sendReady2(){};
        virtual void isMetal(){};
        virtual void puckExit(){};
        virtual void switchTimeout(){};
        virtual void ramp1Full(){};
        virtual void ramp1Accepting(){};
        virtual void ramp2Full(){};
        virtual void ramp2Accepting(){};
        virtual void timeout(){};
        virtual void updateTimer(){};
        virtual void error(){};
        virtual void error2(){};
        virtual void ok(){};
        virtual void ok2(){};
        virtual void resetButton(){};
        virtual void errorGone(){};
        virtual void goToIdle(){};
        virtual void estop(){};

    } * state;

    struct OK : public State {

        virtual void error() {
            if (Dispatcher::error_flag) {
                return;
            }
            this->motorSpeedLevel = Actuators::getInstance()->motorGetSpeedLevel();
            Dispatcher::error_flag = true;
            Timer::getInstance()->stopAllTimers();
            Actuators::getInstance()->setTrafficLightBlinkSpeed(Actuators::RED, Actuators::BLINK_FAST);
            Actuators::getInstance()->trafficLightOff(Actuators::GREEN);
            Actuators::getInstance()->motorStop();
            new (this) PendingNotAcknowledged;
        }

        OK() {
            Dispatcher* disp = Dispatcher::getInstance();
            disp->addListeners(this, ERROR);
        }
    };

    struct PendingNotAcknowledged : public State {

        virtual void resetButton() {
            Actuators::getInstance()->setTrafficLightBlinkSpeed(Actuators::RED, Actuators::BLINK_NONE);
            Actuators::getInstance()->trafficLightOn(Actuators::RED);
            new (this) PendingAcknowledged;
        }

        virtual void errorGone() {
            Actuators::getInstance()->setTrafficLightBlinkSpeed(Actuators::RED, Actuators::BLINK_SLOW);
            new (this) GoneNotAcknowledged;
        }

        PendingNotAcknowledged() {
            Dispatcher* disp = Dispatcher::getInstance();
            disp->addListeners(this, RESET_BUTTON);
            disp->addListeners(this, ERROR_GONE);
        }
    };

    struct PendingAcknowledged : public State {

        virtual void resetButton() {
            if (!Actuators::getInstance()->isInInitState()) {
                Actuators::getInstance()->trafficLightOff(Actuators::GREEN);
                Actuators::getInstance()->trafficLightOn(Actuators::AMBER);
                Actuators::getInstance()->trafficLightOff(Actuators::RED);
                return;
            }

            Actuators::getInstance()->trafficLightOff(Actuators::AMBER);
            Actuators::getInstance()->trafficLightOn(Actuators::GREEN);
            Actuators::getInstance()->trafficLightOff(Actuators::RED);
#ifdef SYSTEM1
            if (PuckData::systemData.ready2) {
                Dispatcher::getInstance()->callListeners(SEND_READY_2);
            }
#endif
#ifdef SYSTEM2
            if (PuckData::systemData.ready1) {
                Dispatcher::getInstance()->callListeners(SEND_READY_1);
            }
#endif

            Dispatcher::error_flag = false;
            Timer::getInstance()->startAllTimers();
            new (this) OK;
        }

        PendingAcknowledged() {
            Dispatcher* disp = Dispatcher::getInstance();

            for (auto p : PuckData::pucks) {
                delete p;
            }

            PuckData::pucks.clear();

            // CLEAR DEMULTIPLEXER VARIABLES
            MsgSendPulse(ConnectAttach(0, 0, HAL::Sensors::getInstance()->get_isrChannel(), 0, 0),
                         SIGEV_PULSE_PRIO_INHERIT, INTERN, 0);

#ifdef SYSTEM1
            PuckData::nextExpectedPuck = PuckData::lastAcceptedPuck;
            cout << "SET NEXT_EXPECTED PUCK TO " << PuckData::lastAcceptedPuck;
#endif
#ifdef SYSTEM2
            SerialCom::getInstance()->sendMessage(SerialCom::SYNCHRONIZE_EXPECTED_PUCK,
                                                  (new PuckData(-1, PuckData::nextExpectedPuck, -1)));
#endif

            Dispatcher::getInstance()->callListeners(GO_TO_IDLE);
            disp->addListeners(this, RESET_BUTTON);
        }
    };

    struct GoneNotAcknowledged : public State {

        virtual void resetButton() {
            Actuators::getInstance()->setTrafficLightBlinkSpeed(Actuators::RED, Actuators::BLINK_NONE);
            Actuators::getInstance()->trafficLightOff(Actuators::RED);
            new (this) OK;
        }

        virtual void error() {
            this->motorSpeedLevel = Actuators::getInstance()->motorGetSpeedLevel();

            Dispatcher::error_flag = true;
            Timer::getInstance()->stopAllTimers();
            Actuators::getInstance()->setTrafficLightBlinkSpeed(Actuators::RED, Actuators::BLINK_FAST);
            Actuators::getInstance()->motorStop();
            new (this) PendingNotAcknowledged;
        }

        GoneNotAcknowledged() {
            Dispatcher::error_flag = false;
            cout << "MotorSpeedLevel ESM: " << this->motorSpeedLevel << endl;
            if (this->motorSpeedLevel != Actuators::NONE) {
                Actuators::getInstance()->motorStart(Actuators::FORWARDS, this->motorSpeedLevel);
            }
#ifdef SYSTEM1
            if (PuckData::systemData.ready2) {
                Dispatcher::getInstance()->callListeners(SEND_READY_2);
            }
#endif
#ifdef SYSTEM2
            if (PuckData::systemData.ready1) {
                Dispatcher::getInstance()->callListeners(SEND_READY_1);
            }
#endif
            Timer::getInstance()->startAllTimers();
            Dispatcher* disp = Dispatcher::getInstance();
            disp->addListeners(this, RESET_BUTTON);
            disp->addListeners(this, ERROR);
        }
    };
};

#endif /* ERRORSTATEMACHINE_H_ */
