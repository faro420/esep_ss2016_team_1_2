/*
 * constants.cpp
 *
 *  Created on: 17.05.2016
 *      Author: abp842
 */

#include "Constants.h"

int Constants::PUCK_OFFSET = 500;

int Constants::FLAT_HEIGHT_MIN = 0;
int Constants::FLAT_HEIGHT_MAX = 0;

int Constants::HOLE_HEIGHT_MIN = 0;
int Constants::HOLE_HEIGHT_MAX = 0;

int Constants::TIMER_OFFSET = 100;

int Constants::TIMER_HEIGHT_MIN = 0;
int Constants::TIMER_SWITCH_MIN = 0;
int Constants::TIMER_EXIT_MIN = 0;

int Constants::TIMER_HEIGHT_MAX = 0;
int Constants::TIMER_SWITCH_MAX = 0;
int Constants::TIMER_EXIT_MAX = 0;

int Constants::PUCK_TICK_FAST = 10000000;
int Constants::PUCK_TICK_SLOW = 10000000;
