/*
 * ErrorStateMachine.cpp
 *
 *  Created on: 02.06.2016
 *      Author: abj718
 */

#include "ErrorStateMachine.h"

ErrorStateMachine::ErrorStateMachine() : state(new OK()) {}

ErrorStateMachine::~ErrorStateMachine() {
    Dispatcher::getInstance()->remListeners(this->state, RESET_BUTTON);
    Dispatcher::getInstance()->remListeners(this->state, ERROR);
    Dispatcher::getInstance()->remListeners(this->state, ERROR_GONE);
    Dispatcher::getInstance()->remListeners(this->state, GO_TO_IDLE);
    // TODO Auto-generated destructor stub
}
