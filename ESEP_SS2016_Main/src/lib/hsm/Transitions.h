/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   Transitions.h
 * Author: Stefan
 *
 * Created on 26. April 2016, 02:07
 */

#ifndef TRANSITIONS_H
#define TRANSITIONS_H

class Transitions {
public:
    virtual void LB_Entry_1() = 0;
    virtual void LB_Entry_2() = 0;
    virtual void LB_Height_1() = 0;
    virtual void LB_Height_2() = 0;
    virtual void LB_Switch_1() = 0;
    virtual void LB_Switch_2() = 0;
    virtual void LB_Ramp_1() = 0;
    virtual void LB_Ramp_2() = 0;
    virtual void LB_Exit_1() = 0;
    virtual void LB_Exit_2() = 0;
    virtual void transferPuck() = 0;
    virtual void transferFinished() = 0;
    virtual void sendReady1() = 0;
    virtual void sendReady2() = 0;
    virtual void isMetal() = 0;
    virtual void puckExit() = 0;
    virtual void switchTimeout() = 0;
    virtual void onHeight() = 0;
    virtual void timeout() = 0;
    // ab hier erstmal unwichtig
    virtual void ramp1Full() = 0;
    virtual void ramp1Accepting() = 0;
    virtual void ramp2Full() = 0;
    virtual void ramp2Accepting() = 0;
    virtual void updateTimer() = 0;
    virtual void error() = 0;
    virtual void error2() = 0;
    virtual void ok() = 0;
    virtual void ok2() = 0;
    virtual void resetButton() = 0;
    virtual void errorGone() = 0;
    virtual void goToIdle() = 0;
    virtual void estop() = 0;

    virtual ~Transitions(){};
};
#endif /* TRANSITIONS_H */
