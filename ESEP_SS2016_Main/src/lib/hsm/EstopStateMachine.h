/*
 * EstopStateMachine.h
 *
 *  Created on: 20.06.2016
 *      Author: abj718
 */

#ifndef ESTOPSTATEMACHINE_H_
#define ESTOPSTATEMACHINE_H_

#include "../SerialCom.h"
#include "../hal/Actuators.h"
#include "../hal/portmap.h"
#include "../hsm/Dispatcher.h"
#include "../timer/Timer.h"
#include "PuckData.h"
#include "PuckStateMachine.h"
#include "Transitions.h"

using namespace HAL;

class EstopStateMachine {
public:
    EstopStateMachine();
    virtual ~EstopStateMachine();

private:
    struct State : public Transitions {
        Actuators::SpeedLevel motorSpeedLevel;

        virtual void LB_Entry_1(){};
        virtual void LB_Entry_2(){};
        virtual void LB_Height_1(){};
        virtual void LB_Height_2(){};
        virtual void LB_Switch_1(){};
        virtual void LB_Switch_2(){};
        virtual void LB_Ramp_1(){};
        virtual void LB_Ramp_2(){};
        virtual void LB_Exit_1(){};
        virtual void LB_Exit_2(){};
        virtual void transferPuck(){};
        virtual void transferFinished(){};
        virtual void sendReady1(){};
        virtual void onHeight(){};
        virtual void sendReady2(){};
        virtual void isMetal(){};
        virtual void puckExit(){};
        virtual void switchTimeout(){};
        virtual void ramp1Full(){};
        virtual void ramp1Accepting(){};
        virtual void ramp2Full(){};
        virtual void ramp2Accepting(){};
        virtual void timeout(){};
        virtual void updateTimer(){};
        virtual void error(){};
        virtual void error2(){};
        virtual void ok(){};
        virtual void ok2(){};
        virtual void resetButton(){};
        virtual void errorGone(){};
        virtual void goToIdle(){};
        virtual void estop(){};

    } * state;

    struct OK : public State {

        virtual void estop() {
            if (!Dispatcher::error_flag) {
                new (this) EstopPressed;
            }
        }

        OK() {
            Dispatcher::getInstance()->addListeners(this, ESTOP);
            Actuators::getInstance()->trafficLightOn(Actuators::GREEN);
        }
    };

    struct EstopPressed : public State {

        virtual void estop() {
            cout << "ESTOP SM ME=" << PuckData::systemData.estop_me
                 << " OTHER=" << PuckData::systemData.estop_other << endl;
            if (!PuckData::systemData.estop_me && !PuckData::systemData.estop_other) {
                new (this) WaitForReset;
            }
        }

        EstopPressed() {
            this->motorSpeedLevel = Actuators::getInstance()->motorGetSpeedLevel();
            Dispatcher::error_flag = true;
            Timer::getInstance()->stopAllTimers();
            Actuators::getInstance()->trafficLightOff(Actuators::GREEN);
            Actuators::getInstance()->setTrafficLightBlinkSpeed(Actuators::RED, Actuators::BLINK_FAST);
            Actuators::getInstance()->motorStop();
        }
    };

    struct WaitForReset : public State {

        virtual void estop() {
            new (this) EstopPressed;
        }

        virtual void resetButton() {
            if (!Actuators::getInstance()->isInInitState()) {
                Actuators::getInstance()->trafficLightOff(Actuators::GREEN);
                Actuators::getInstance()->trafficLightOn(Actuators::AMBER);
                Actuators::getInstance()->setTrafficLightBlinkSpeed(Actuators::RED, Actuators::BLINK_NONE);
                Actuators::getInstance()->trafficLightOff(Actuators::RED);
            }

            Actuators::getInstance()->trafficLightOff(Actuators::AMBER);
            Actuators::getInstance()->trafficLightOn(Actuators::GREEN);
            SerialCom::getInstance()->sendMessage(SerialCom::RESET, 0);
            Actuators::getInstance()->setTrafficLightBlinkSpeed(Actuators::RED, Actuators::BLINK_NONE);
            Actuators::getInstance()->trafficLightOff(Actuators::RED);

            for (auto p : PuckData::pucks) {
                delete p;
            }

            PuckData::pucks.clear();

            // CLEAR DEMULTIPLEXER VARIABLES
            MsgSendPulse(ConnectAttach(0, 0, HAL::Sensors::getInstance()->get_isrChannel(), 0, 0),
                         SIGEV_PULSE_PRIO_INHERIT, INTERN, 0);

#ifdef SYSTEM1
            PuckData::nextExpectedPuck = PuckData::lastAcceptedPuck;
            cout << "SET NEXT_EXPECTED PUCK TO " << PuckData::lastAcceptedPuck;
#endif
#ifdef SYSTEM2
            SerialCom::getInstance()->sendMessage(SerialCom::SYNCHRONIZE_EXPECTED_PUCK,
                                                  (new PuckData(-1, PuckData::nextExpectedPuck, -1)));
#endif

            Dispatcher::getInstance()->callListeners(GO_TO_IDLE);

            Timer::getInstance()->startAllTimers();

            Dispatcher::error_flag = false;
            Dispatcher::getInstance()->remListeners(this, RESET_BUTTON);
            Dispatcher::getInstance()->remListeners(this, ESTOP);
            new (this) OK;
        }

        WaitForReset() {
            Actuators::getInstance()->setTrafficLightBlinkSpeed(Actuators::RED, Actuators::BLINK_NONE);
            Actuators::getInstance()->trafficLightOff(Actuators::RED);
            Dispatcher::getInstance()->addListeners(this, RESET_BUTTON);
        }
    };
};

#endif /* ESTOPSTATEMACHINE_H_ */
