/**
 * This is the Dispatcher which distributes the actions to the respective
 * registered context objects.
 * @author Stefan Sylvius Wagner
 * @date 26.04.2016
 */
#ifndef DISPATCHER_H
#define DISPATCHER_H
#include "Transitions.h"
#include <iostream>

#define MAXLISTENERS 500

//! Events that can be called for the Dispatcher to execute.
enum EVENTS {
    LB_ENTRY_1,
    LB_ENTRY_2,
    LB_HEIGHT_1,
    LB_HEIGHT_2,
    LB_SWITCH_1,
    LB_SWITCH_2,
    LB_RAMP_1,
    LB_RAMP_2,
    LB_EXIT_1,
    LB_EXIT_2,
    HEIGHT,
    TRANSFER_PUCK,
    TRANSFER_FINISHED,
    SEND_READY_1,
    SEND_READY_2,
    IS_METAL,
    PUCK_EXIT,
    RAMP_1_FULL,
    RAMP_1_ACCEPTING,
    RAMP_2_FULL,
    RAMP_2_ACCEPTING,
    TIMEOUT,
    UPDATE_TIMER,
    ERROR,
    ERROR_2,
    OK,
    OK_2,
    SWITCH_TIMEOUT,
    RESET_BUTTON,
    ERROR_GONE,
    GO_TO_IDLE,
    ESTOP,
    NEVENTS
};
//! Custom function pointer to map each transition function to an event.
//! See Transition.h for interface definition.
typedef void (Transitions::*stateMethods)(void);

class Dispatcher {
public:
    static Dispatcher* getInstance();
    /**
     * This function binds a Listener to an event for the dispatcher to call.
     * @param Transitions* An object of the base class Transitions.
     * @param EVENTS The event for which the listener is to be registered.
     */
    void addListeners(Transitions*, EVENTS);
    /**
     * This function removes the binding between the passed listener and event.
     * @param Transitions*  An object compatible with type Transitions.
     * @param EVENTS The event for which the listener is to be removed.
     */
    void remListeners(Transitions*, EVENTS);
    /**
     * This function calls all listeners registered to the passed event.
     * @param EVENTS Event to be called for listeners
     */
    void callListeners(EVENTS);

    static bool error_flag;

private:
    stateMethods statesMethods[NEVENTS];
    Transitions* listeners_[NEVENTS][MAXLISTENERS];

    Dispatcher() {
        for (int i = 0; i < NEVENTS; i++) {
            for (int j = 0; j < MAXLISTENERS; j++) {
                listeners_[i][j] = NULL;
            }
        }

        statesMethods[LB_ENTRY_1] = &Transitions::LB_Entry_1;
        statesMethods[LB_ENTRY_2] = &Transitions::LB_Entry_2;
        statesMethods[LB_HEIGHT_1] = &Transitions::LB_Height_1;
        statesMethods[LB_HEIGHT_2] = &Transitions::LB_Height_2;
        statesMethods[LB_SWITCH_1] = &Transitions::LB_Switch_1;
        statesMethods[LB_SWITCH_2] = &Transitions::LB_Switch_2;
        statesMethods[LB_RAMP_1] = &Transitions::LB_Ramp_1;
        statesMethods[LB_RAMP_2] = &Transitions::LB_Ramp_2;
        statesMethods[LB_EXIT_1] = &Transitions::LB_Exit_1;
        statesMethods[LB_EXIT_2] = &Transitions::LB_Exit_2;
        statesMethods[HEIGHT] = &Transitions::onHeight;
        statesMethods[TRANSFER_PUCK] = &Transitions::transferPuck;
        statesMethods[TRANSFER_FINISHED] = &Transitions::transferFinished;
        statesMethods[SEND_READY_1] = &Transitions::sendReady1;
        statesMethods[SEND_READY_2] = &Transitions::sendReady2;
        statesMethods[IS_METAL] = &Transitions::isMetal;
        statesMethods[PUCK_EXIT] = &Transitions::puckExit;
        statesMethods[SWITCH_TIMEOUT] = &Transitions::switchTimeout;
        statesMethods[RAMP_1_FULL] = &Transitions::ramp1Full;
        statesMethods[RAMP_1_ACCEPTING] = &Transitions::ramp1Accepting;
        statesMethods[RAMP_2_FULL] = &Transitions::ramp2Full;
        statesMethods[RAMP_2_ACCEPTING] = &Transitions::ramp2Accepting;
        statesMethods[TIMEOUT] = &Transitions::timeout;
        statesMethods[UPDATE_TIMER] = &Transitions::updateTimer;
        statesMethods[ERROR] = &Transitions::error;
        statesMethods[ERROR_2] = &Transitions::error2;
        statesMethods[OK] = &Transitions::ok;
        statesMethods[OK_2] = &Transitions::ok2;
        statesMethods[RESET_BUTTON] = &Transitions::resetButton;
        statesMethods[GO_TO_IDLE] = &Transitions::goToIdle;
        statesMethods[ERROR_GONE] = &Transitions::errorGone;
        statesMethods[ESTOP] = &Transitions::estop;
    }
    ~Dispatcher();

    Dispatcher(const Dispatcher& other);
    Dispatcher& operator=(const Dispatcher& other);
};

#endif /* DISPATCHER_H */
