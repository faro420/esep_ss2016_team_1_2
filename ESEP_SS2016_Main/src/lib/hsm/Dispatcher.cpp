/**
 * This is the Dispatcher which distributes the actions to the respective
 * registered context objects.
 * @author Stefan Sylvius Wagner
 * @date 26.04.2016
 */

#include "Dispatcher.h"
using namespace std;

Dispatcher::~Dispatcher() {}

bool Dispatcher::error_flag = false;

Dispatcher* Dispatcher::getInstance(void) {
    static Dispatcher instance_;
    return &instance_;
}

void Dispatcher::addListeners(Transitions* listener, EVENTS event) {
    for (int i = 0; i < MAXLISTENERS; i++) {
        if (listeners_[event][i] == NULL || listeners_[event][i] == listener) {
            listeners_[event][i] = listener;
            return;
        }
    }
}

void Dispatcher::remListeners(Transitions* listener, EVENTS event) {
    for (int i = 0; i < MAXLISTENERS; i++) {
        if (listeners_[event][i] == listener) {
            listeners_[event][i] = NULL;
            return;
        }
    }
}

void Dispatcher::callListeners(EVENTS event) {
    for (int i = 0; i < MAXLISTENERS; i++) {
        if (listeners_[event][i] != NULL) {
            (listeners_[event][i]->*statesMethods[event])();
        }
    }
}
