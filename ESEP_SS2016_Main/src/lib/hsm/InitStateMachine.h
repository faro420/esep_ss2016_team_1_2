/*
 * InitStateMachine.h
 *
 *  Created on: 27.05.2016
 *      Author: abp842
 */

#ifndef INITSTATEMACHINE_H_
#define INITSTATEMACHINE_H_

#include "../hal/Actuators.h"
#include "../hal/Sensors.h"
#include "../timer/Timer.h"
#include "Constants.h"
#include "Dispatcher.h"
#include "PuckData.h"
#include "Transitions.h"
#include <iostream>
#include <unistd.h>

using namespace std;
using namespace HAL;

class InitStateMachine {
public:
    InitStateMachine(PuckData* puck);
    virtual ~InitStateMachine();

private:
    struct State : public Transitions {
        virtual void LB_Entry_1(){};
        virtual void LB_Entry_2(){};
        virtual void LB_Height_1(){};
        virtual void LB_Height_2(){};
        virtual void LB_Switch_1(){};
        virtual void LB_Switch_2(){};
        virtual void LB_Ramp_1(){};
        virtual void LB_Ramp_2(){};
        virtual void LB_Exit_1(){};
        virtual void LB_Exit_2(){};
        virtual void transferPuck(){};
        virtual void transferFinished(){};
        virtual void sendReady1(){};
        virtual void onHeight(){};
        virtual void sendReady2(){};
        virtual void isMetal(){};
        virtual void puckExit(){};
        virtual void switchTimeout(){};
        virtual void ramp1Full(){};
        virtual void ramp1Accepting(){};
        virtual void ramp2Full(){};
        virtual void ramp2Accepting(){};
        virtual void timeout(){};
        virtual void updateTimer(){};
        virtual void error(){};
        virtual void error2(){};
        virtual void ok(){};
        virtual void ok2(){};
        virtual void resetButton(){};
        virtual void errorGone(){};
        virtual void goToIdle(){};
        virtual void estop(){};
    } * state;

    struct Idle : public State {
        PuckData* puck;

        virtual void LB_Entry_1() {
            Actuators::getInstance()->motorStart(Actuators::FORWARDS, Actuators::FAST);
            Actuators::getInstance()->trafficLightOn(Actuators::AMBER);
            new (this) Entry(puck);
        }

        Idle(PuckData* puck) : puck(puck) {
            Dispatcher::getInstance()->addListeners(this, LB_ENTRY_1);
        }
    };

    /*
     *
     * ENTRY STATE
     *
     */
    struct Entry : public State {
        PuckData* puck;

        virtual void LB_Height_1() {
            if (puck->GetID() == 1) {
                // cout << "INITIALIZE_STATE:: height_1 timer min " << puck->getTicks() << endl;
                Constants::TIMER_HEIGHT_MIN = puck->getTicks() - Constants::TIMER_OFFSET;
            } else if (puck->GetID() == 2) {
                // cout << "INITIALIZE_STATE:: height_1 timer max " << puck->getTicks() << endl;
                Constants::TIMER_HEIGHT_MAX = puck->getTicks() + Constants::TIMER_OFFSET;
            }

            puck->resetTicks();
            new (this) HeightMeasurement(puck);
        }

        Entry(PuckData* puck) : puck(puck) {
            Dispatcher::getInstance()->addListeners(this, UPDATE_TIMER);
            Dispatcher::getInstance()->addListeners(this, LB_HEIGHT_1);
        }

        virtual void updateTimer() {
            puck->incrementTicks();
        }
    };

    /*
     *
     * HEIGHT MEAUSERMENT STATE
     *
     */
    struct HeightMeasurement : public State {
        PuckData* puck;

        virtual void LB_Switch_1() {
            if (puck->GetID() == 1) {
                // cout << "INITIALIZE_STATE:: switch_1 timer min " << puck->getTicks() << endl;
                Constants::TIMER_SWITCH_MIN = puck->getTicks() - Constants::TIMER_OFFSET;
            } else if (puck->GetID() == 2) {
                // cout << "INITIALIZE_STATE:: switch_1 timer max " << puck->getTicks() << endl;
                Constants::TIMER_SWITCH_MAX = puck->getTicks() + Constants::TIMER_OFFSET;
            }

            puck->resetTicks();

            Actuators::getInstance()->switchOpen();
            Timer::getInstance()->createTimer(Timer::SWITCH, 1, 0, 0, false, false);
            Dispatcher::getInstance()->addListeners(this, SWITCH_TIMEOUT);

            new (this) Accepted(puck);
        }

        virtual void LB_Height_2() {
            Actuators::getInstance()->motorStart(Actuators::FORWARDS, Actuators::FAST);
        }

        virtual void isMetal() {
            cout << "INITIALIZE_STATE:: received metal detector signal" << endl;
        }

        virtual void updateTimer() {
            puck->incrementTicks();
        }

        HeightMeasurement(PuckData* puck) : puck(puck) {
            Actuators::getInstance()->motorStart(Actuators::FORWARDS, Actuators::SLOW);
            Dispatcher::getInstance()->remListeners(this, LB_HEIGHT_1);
            Dispatcher::getInstance()->addListeners(this, LB_HEIGHT_2);
            Dispatcher::getInstance()->addListeners(this, IS_METAL);
            Dispatcher::getInstance()->addListeners(this, LB_SWITCH_1);

            uint16_t height = Sensors::getInstance()->getSensorHeight();
            if (puck->GetID() == 1) {
                Constants::FLAT_HEIGHT_MIN = height - Constants::PUCK_OFFSET;
                Constants::FLAT_HEIGHT_MAX = height + Constants::PUCK_OFFSET;

                cout << "INITIALIZE_STATE:: height flat min=" << Constants::FLAT_HEIGHT_MIN
                     << " max=" << Constants::FLAT_HEIGHT_MAX << endl;
            } else if (puck->GetID() == 2) {
                Constants::HOLE_HEIGHT_MIN = height - Constants::PUCK_OFFSET;
                Constants::HOLE_HEIGHT_MAX = height + Constants::PUCK_OFFSET;

                //	cout << "INITIALIZE_STATE:: height hole min=" << Constants::HOLE_HEIGHT_MIN << " max=" <<
                // Constants::HOLE_HEIGHT_MAX << endl;
            }
        }
    };

    /*
     *
     * ACCEPTED STATE
     *
     */
    struct Accepted : public State {
        PuckData* puck;

        virtual void switchTimeout() {
            Actuators::getInstance()->switchClose();
            Dispatcher::getInstance()->remListeners(this, SWITCH_TIMEOUT);
        }

        virtual void updateTimer() {
            puck->incrementTicks();
        }

        virtual void LB_Exit_1() {
            if (puck->GetID() == 1) {
                // cout << "INITIALIZE_STATE:: exit_1 timer min " << puck->getTicks() << endl;
                Constants::TIMER_EXIT_MIN = puck->getTicks() - Constants::TIMER_OFFSET;
            } else if (puck->GetID() == 2) {
                // cout << "INITIALIZE_STATE:: exit_1 timer max " << puck->getTicks() << endl;
                Constants::TIMER_EXIT_MAX = puck->getTicks() + Constants::TIMER_OFFSET;
            }

            cout << "INITIALIZE_STATE:: take puck off the line" << endl;

            Actuators::getInstance()->motorStop();
            Actuators::getInstance()->trafficLightOff(Actuators::AMBER);
            Actuators::getInstance()->trafficLightOn(Actuators::RED);

            Dispatcher::getInstance()->remListeners(this, UPDATE_TIMER);
            Dispatcher::getInstance()->remListeners(this, LB_EXIT_1);
        }

        virtual void LB_Exit_2() {
            cout << "INITIALIZE_STATE:: puck off the line" << endl;
            Dispatcher::getInstance()->callListeners(PUCK_EXIT);
            Actuators::getInstance()->trafficLightOff(Actuators::RED);
            Dispatcher::getInstance()->remListeners(this, LB_EXIT_2);
        }

        Accepted(PuckData* puck) : puck(puck) {
            Dispatcher::getInstance()->remListeners(this, LB_HEIGHT_2);
            Dispatcher::getInstance()->remListeners(this, IS_METAL);
            Dispatcher::getInstance()->remListeners(this, LB_SWITCH_1);
            Dispatcher::getInstance()->addListeners(this, LB_EXIT_1);
            Dispatcher::getInstance()->addListeners(this, LB_EXIT_2);
        }
    };
};

#endif /* INITSTATEMACHINE_H_ */
