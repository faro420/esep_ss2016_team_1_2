/*
 * Demultiplexer.h
 *
 *  Created on: 13.05.2016
 *      Author: abp842
 */

#ifndef DEMULTIPLEXER_H_
#define DEMULTIPLEXER_H_
#include "../HAWThread.h"
#include "ErrorStateMachine.h"
#include "EstopStateMachine.h"
#include "SystemStateMachine.h"
using namespace thread;

class Demultiplexer : public HAWThread {
private:
    bool lb_entry;
    bool lb_height;
    bool height;
    bool lb_switch;
    bool metall_detector;
    bool lb_ramp;
    bool lb_exit;
    bool button_start;
    bool button_stop;
    bool button_reset;
    bool button_estop;
    bool estop_active;
    int tick_timer;
    bool first_heartbeat;

    SystemStateMachine* system;
    ErrorStateMachine* error;
    EstopStateMachine* estop;
    timer_t estopTimer;

    void clear() {
        lb_entry = false;
        lb_height = false;
        height = false;
        lb_switch = false;
        metall_detector = false;
        // lb_ramp = false;
        lb_exit = false;
        first_heartbeat = false;
    }

    Demultiplexer();
    virtual ~Demultiplexer();

public:
    bool init;

    void getNextMessage();
    void resetSystem();

    virtual void execute(void*);
    virtual void shutdown();

    static Demultiplexer* getInstance() {
        static Demultiplexer instance_;

        return &instance_;
    }
};

#endif /* DEMULTIPLEXER_H_ */
