
#include "TimeKeeper.h"
#include "../Logger.h"
#include "../hal/Actuators.h"
#include "Constants.h"
#include "Dispatcher.h"
#include "PuckData.h"
#include "PuckStateMachine.h"

TimeKeeper::TimeKeeper() : currentTimeout_(TIMEOUT_HM) {}
TimeKeeper::~TimeKeeper() {}

TimeKeeper* TimeKeeper::getInstance() {
    static TimeKeeper instance_;
    return &instance_;
}

bool TimeKeeper::checkTimeout() {
    cout << "currentTImeout " << this->currentTimeout_ << endl;
    if (PuckData::pucks.empty()) {
        return true;
    }
    // should be true if no puck in pucks is in the right state
    bool puckNotFound = true;
    switch (this->currentTimeout_) {
        case TIMEOUT_HM:
            for (auto& p : PuckData::pucks) {

                if (p->state->currentPuck->getState() == HEIGHT_MEASUREMENT) {
                    cout << "State Puck: " << p->state->currentPuck->getState()
                         << " PID: " << p->state->currentPuck->GetID() << endl;
                    puckNotFound = false;
                    if (!(p->state->currentPuck->getTicks() > Constants::TIMER_HEIGHT_MIN &&
                          p->state->currentPuck->getTicks() < Constants::TIMER_HEIGHT_MAX)) {

                        cout << "Invalid Puck Detected in Height Measuerment" << endl;
                        cout << "HEIGHT MIN: " << Constants::TIMER_HEIGHT_MIN << endl;
                        cout << "HEIGHT MAX: " << Constants::TIMER_HEIGHT_MAX << endl;
                        cout << "TICKS PUCK: " << p->state->currentPuck->getTicks() << endl;

                        return true;
                    }

                    p->state->currentPuck->setChangedState(true);
                    p->state->currentPuck->resetTicks();
                    p->state->currentPuck->setState(SWITCH);
                    break;
                }
            }
            if (puckNotFound) {
                // cout << "Not found HM" << endl;
                return true;
            }
            break;
        case TIMEOUT_SWITCH:
            for (auto& p : PuckData::pucks) {

                if (p->state->currentPuck->getState() == SWITCH) {
                    cout << "State Puck: " << p->state->currentPuck->getState()
                         << " PID: " << p->state->currentPuck->GetID() << endl;

                    puckNotFound = false;
                    if (!(p->state->currentPuck->getTicks() > Constants::TIMER_SWITCH_MIN &&
                          p->state->currentPuck->getTicks() < Constants::TIMER_SWITCH_MAX)) {
                        cout << "Invalid Puck Detected in Switch" << endl;

                        cout << "HEIGHT MIN: " << Constants::TIMER_SWITCH_MIN << endl;
                        cout << "HEIGHT MAX: " << Constants::TIMER_SWITCH_MAX << endl;
                        cout << "TICKS PUCK: " << p->state->currentPuck->getTicks() << endl;
                        return true;
                    }
                    p->state->currentPuck->setChangedState(true);
                    p->state->currentPuck->resetTicks();
                    p->state->currentPuck->setState(EXIT);
                    break;
                }
            }
            if (puckNotFound) {
                // cout << "puck not found" << endl;
                return true;
            }
            break;
        case TIMEOUT_EXIT:
            for (auto& p : PuckData::pucks) {
                cout << "State Puck: " << p->state->currentPuck->getState()
                     << " PID: " << p->state->currentPuck->GetID() << endl;

                if (p->state->currentPuck->getState() == EXIT) {

                    puckNotFound = false;
                    if (!(p->state->currentPuck->getTicks() > Constants::TIMER_EXIT_MIN &&
                          p->state->currentPuck->getTicks() < Constants::TIMER_EXIT_MAX)) {

                        cout << "Invalid Puck Detected in EXIT" << endl;
                        cout << "EXIT MIN: " << Constants::TIMER_EXIT_MIN << endl;
                        cout << "EXIT MAX: " << Constants::TIMER_EXIT_MAX << endl;
                        cout << "TICKS PUCK: " << p->state->currentPuck->getTicks() << endl;
                        cout << "ID PUCK: " << p->state->currentPuck->GetID() << endl;
                        return true;
                    }
                    p->state->currentPuck->setChangedState(true);
                    p->state->currentPuck->setState(NO_STATE);
                    break;
                }
            }
            if (puckNotFound) {
                cout << "puck not found" << endl;
                return true;
            }
            break;
        default:
            cout << "Unknown Timeout type" << endl;
    }
    cout << "Puck accepted" << endl;
    return false;
}

void TimeKeeper::callTimeout(Timeouts timeout) {
    this->currentTimeout_ = timeout;
    Dispatcher::getInstance()->callListeners(TIMEOUT);
}
