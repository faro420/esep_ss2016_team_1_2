#/*
 * PuckStateMachine.h
 *
 *  Created on: 13.05.2016
 *      Author: abp842
 */

#ifndef PUCKSTATEMACHINE_H_
#define PUCKSTATEMACHINE_H_

#include "../Logger.h"
#include "../SerialListener.h"
#include "../hal/Actuators.h"
#include "../hal/Sensors.h"
#include "../timer/Timer.h"
#include "Constants.h"
#include "Dispatcher.h"
#include "PuckData.h"
#include "TimeKeeper.h"
#include "Transitions.h"
#include <unistd.h>

using namespace HAL;
using namespace Logger;

class PuckStateMachine {
public:
    PuckStateMachine(PuckData* puck);

    ~PuckStateMachine();

    struct State : public Transitions {
        // TODO PuckData puck can be replaced by currentPuck
        PuckData* currentPuck;
        timer_t switchTimer;

        virtual void LB_Entry_1(){};
        virtual void LB_Entry_2(){};
        virtual void LB_Height_1(){};
        virtual void LB_Height_2(){};
        virtual void LB_Switch_1(){};
        virtual void LB_Switch_2(){};
        virtual void LB_Ramp_1(){};
        virtual void LB_Ramp_2(){};
        virtual void LB_Exit_1(){};
        virtual void LB_Exit_2(){};
        virtual void transferPuck(){};
        virtual void transferFinished(){};
        virtual void sendReady1(){};
        virtual void onHeight(){};
        virtual void sendReady2(){};
        virtual void isMetal(){};
        virtual void puckExit(){};
        virtual void switchTimeout(){};
        virtual void ramp1Full(){};
        virtual void ramp1Accepting(){};
        virtual void ramp2Full(){};
        virtual void ramp2Accepting(){};
        virtual void timeout(){};
        virtual void updateTimer(){};
        virtual void error(){};
        virtual void error2(){};
        virtual void ok(){};
        virtual void ok2(){};
        virtual void resetButton(){};
        virtual void errorGone(){};
        virtual void goToIdle(){};
        virtual void estop(){};
    } * state;

private:
    /*
     *
     * ENTRY STATE
     *
     */
    struct Entry : public State {
        PuckData* puck;
        virtual void LB_Height_1() {

            cout << "height_1 timer " << puck->getTicks() << " PuckID: " << puck->GetID() << endl;
            if (puck->isChangedState()) {
                // puck->resetTicks();
                puck->setChangedState(false);
                new (this) HeightMeasurement(puck);
            } else {
                cout << "Puck" << puck->GetID() << ": LB_Height_1 Ticks " << puck->getTicks() << endl;
            }
        }

        virtual void onHeight() {
            if (puck->getTicks() > (Constants::TIMER_HEIGHT_MIN - 30)) {
#ifdef SYSTEM1
                puck->SetPucktype1(UPSIDEDOWN);
                puck->SetHeight1(Sensors::getInstance()->getSensorHeight());
#endif

#ifdef SYSTEM2
                puck->SetPucktype2(UPSIDEDOWN);
                puck->SetHeight2(Sensors::getInstance()->getSensorHeight());
#endif
                Dispatcher::getInstance()->remListeners(this, HEIGHT);
            }
        }

        Entry(PuckData* puck) : puck(puck) {
            this->currentPuck = puck;
            // TODO
            cout << "setStateHM_ timer " << puck->getTicks() << endl;
            puck->setState(HEIGHT_MEASUREMENT);
            Dispatcher::getInstance()->addListeners(this, UPDATE_TIMER);
            Dispatcher::getInstance()->addListeners(this, LB_HEIGHT_1);
            Dispatcher::getInstance()->addListeners(this, HEIGHT);

#ifdef SYSTEM2
            Dispatcher::getInstance()->addListeners(this, LB_ENTRY_1);

            PuckData::systemData.ready1 = false;
            cout << "ready " << PuckData::systemData.ready1 << endl;
#endif
        }

#ifdef SYSTEM2
        virtual void LB_Entry_1() {
            // TODO checkTimer
            SerialCom::getInstance()->sendMessage(SerialCom::TRANSFER_FINISHED, 0);
            cout << "sending: transfer finished" << endl;
        }
#endif

        virtual void updateTimer() {
            puck->incrementTicks();
            if (puck->getTicks() > Constants::TIMER_HEIGHT_MAX) {
                Dispatcher::getInstance()->callListeners(ERROR);
                cout << "TIMER HEIGHT UPDATE ERROR " << endl;
            }
        }
    };

    /*
     *
     * HEIGHT MEAUSERMENT STATE
     *
     */
    struct HeightMeasurement : public State {
        PuckData* puck;

        virtual void LB_Switch_1() {
            //	cout << "switch_1 timer " << puck->getTicks() << endl;
            if (puck->isChangedState()) {
                // puck->resetTicks();
                puck->setChangedState(false);
                new (this) Evaluation(puck);
            } else {
                cout << "Puck" << puck->GetID() << ": LB_Switch_1 Ticks " << puck->getTicks() << endl;
            }
        }

        virtual void LB_Height_2() {
            cout << ">>>>LB_HEIGHT_2 MOTOR FAST" << endl;
            Actuators::getInstance()->motorStart(Actuators::FORWARDS, Actuators::FAST);
        }

        virtual void isMetal() {
#ifdef SYSTEM1
            if (puck->GetPucktype1() == HOLE_WITHOUT_METAL) {
                puck->SetPucktype1(HOLE_WITH_METAL);
            }
#endif

#ifdef SYSTEM2
            if (puck->GetPucktype2() == HOLE_WITHOUT_METAL) {
                puck->SetPucktype2(HOLE_WITH_METAL);
            }
#endif
        }

        virtual void updateTimer() {

            puck->incrementTicks();
            if (puck->getTicks() > Constants::TIMER_SWITCH_MAX) {
                Dispatcher::getInstance()->callListeners(ERROR);
                cout << "TIMER SWITCH UPDATE ERROR " << endl;
            }
        }

        HeightMeasurement(PuckData* puck) : puck(puck) {
            Actuators::getInstance()->motorStart(Actuators::FORWARDS, Actuators::SLOW);
            Timer::getInstance()->modifyTimer(PuckData::switchTimer, 0, 1500000000, false);

            Dispatcher::getInstance()->remListeners(this, LB_HEIGHT_1);
            Dispatcher::getInstance()->addListeners(this, LB_HEIGHT_2);
            Dispatcher::getInstance()->addListeners(this, LB_SWITCH_1);
            Dispatcher::getInstance()->addListeners(this, IS_METAL);

#ifdef SYSTEM1
            if (Sensors::getInstance()->isInHeightMeassurementTolerance()) {
                if (puck->GetPucktype1() == UNDEFINED) {
                    puck->SetHeight1(Sensors::getInstance()->getSensorHeight());
                    puck->SetPucktype1(FLAT);
                } else if (puck->GetPucktype1() == UPSIDEDOWN) {
                    puck->SetPucktype1(HOLE_WITHOUT_METAL);
                }
            }
#endif
#ifdef SYSTEM2
            if (Sensors::getInstance()->isInHeightMeassurementTolerance()) {
                if (puck->GetPucktype2() == UNDEFINED) {
                    puck->SetHeight2(Sensors::getInstance()->getSensorHeight());
                    puck->SetPucktype2(FLAT);
                } else if (puck->GetPucktype2() == UPSIDEDOWN) {
                    puck->SetPucktype2(HOLE_WITHOUT_METAL);
                }
            }
#endif
        }
    };
    /*
     *
     * EVALUATION STATE
     *
     */
    struct Evaluation : public State {
        PuckData* puck;

        Evaluation(PuckData* puck) : puck(puck) {
            // TODO
            // puck->setState(SWITCH);
            Dispatcher::getInstance()->remListeners(this, LB_HEIGHT_2);
            Dispatcher::getInstance()->remListeners(this, LB_SWITCH_1);
            Dispatcher::getInstance()->remListeners(this, IS_METAL);
            Dispatcher::getInstance()->addListeners(this, LB_SWITCH_2);
            Dispatcher::getInstance()->addListeners(this, SWITCH_TIMEOUT);

#ifdef SYSTEM1
            if (PuckData::systemData.ramp1Full) {
                remove(1);
                new (this) Accepted(puck);
            } else if (PuckData::systemData.ramp2Full && remove(1)) {
                new (this) ToBeDismissed(puck);
            } else if (!PuckData::systemData.ramp1Full && !PuckData::systemData.ramp2Full && remove(0)) {
                new (this) ToBeDismissed(puck);
#endif

#ifdef SYSTEM2
                if (remove(1)) {
                    if (PuckData::systemData.ramp2Full) {
                        Dispatcher::getInstance()->callListeners(ERROR);
                    } else {
                        new (this) ToBeDismissed(puck);
                    }
#endif

                } else {

#ifdef SYSTEM2
                    if (puck->GetPucktype2() == HOLE_WITH_METAL) {
                        SerialCom::getInstance()->sendMessage(SerialCom::METAL_DETECTED, 0);
                    }
#endif
                    new (this) Accepted(puck);
                }
            }

            bool remove(int mode) {
#ifdef SYSTEM1
                if (mode == 1) {
                    if (PuckData::nextExpectedPuck == puck->GetPucktype1()) {
                        switch (PuckData::nextExpectedPuck) {
                            case FLAT:
                                PuckData::nextExpectedPuck = HOLE_WITHOUT_METAL;
                                break;
                            case HOLE_WITHOUT_METAL:
                                PuckData::nextExpectedPuck = HOLE_WITH_METAL;
                                break;
                            case HOLE_WITH_METAL:
                                PuckData::nextExpectedPuck = FLAT;
                                break;
                            default:
                                break;
                        }
                        return false;
                    }
                    return true;
                } else {
                    if (puck->GetPucktype1() == UNDEFINED) {
                        return true;
                    }

                    if (puck->GetPucktype1() == HOLE_WITH_METAL) {
                        // cout << "let whole with metal through" << endl;
                        return false;
                    }

                    if (PuckData::nextExpectedPuck == puck->GetPucktype1() &&
                        (puck->GetPucktype1() == HOLE_WITHOUT_METAL ||
                         puck->GetPucktype1() == HOLE_WITH_METAL)) {
                        // cout << "let whole without metal through" << endl;
                        return false;
                    }

                    if (PuckData::nextExpectedPuck == puck->GetPucktype1() && puck->GetPucktype1() == FLAT) {
                        // cout << "let flat through" << endl;
                        PuckData::nextExpectedPuck = HOLE_WITHOUT_METAL;
                        return false;
                    }

                    // cout << "sort out, next expected:" << PuckData::nextExpectedPuck << " got:" <<
                    // puck->GetPucktype() << endl;
                    return true;
                }

#endif

#ifdef SYSTEM2
                // deprecated durch synchronize_expected_puck
                //			if (puck->GetPucktype1() != puck->GetPucktype2()){
                //				SerialCom::getInstance()->sendMessage(SerialCom::ROLLBACK_EXPECTED_PUCK,
                //NULL);
                //				return true;
                //			}

                cout << "CHECK PUCKTYPE2 = " << puck->GetPucktype2() << endl;
                if (PuckData::nextExpectedPuck == puck->GetPucktype2()) {
                    return false;
                }
                return true;
#endif
            }
        };
        /*
         *
         * TOBEDISMISSED STATE
         *
         */
        struct ToBeDismissed : public State {
            PuckData* puck;

            virtual void switchTimeout() {
                Actuators::getInstance()->switchClose();
            }

            virtual void LB_Ramp_1() {
                cout << "LB_RAMP_1 UNTERBROCHEN PUCK " << puck->GetID() << " STATE " << puck->getState()
                     << endl;
                new (this) Ramp_Entry(puck);
                //            for (auto &p : PuckData::pucks) {
                //                if (p->state->currentPuck->getState() == EXIT) {
                //                    if (p->state->currentPuck->GetID() == puck->GetID()) {
                //                        new (this) Ramp_Entry(puck);
                //                    }
                //                    break;
                //                }
                //            }
            }

            ToBeDismissed(PuckData* puck) : puck(puck) {
                cout << "*****************" << endl;
                cout << "IN TO_BE_DISMISSED" << endl;
                puck->printPuckData();
                cout << "*****************" << endl;
                Dispatcher::getInstance()->remListeners(this, LB_SWITCH_2);
                Dispatcher::getInstance()->addListeners(this, LB_RAMP_1);
            }
        };
        /*
         *
         * RAMP_ENTRY STATE
         *
         */
        struct Ramp_Entry : public State {
            PuckData* puck;

            virtual void LB_Ramp_2() {
                if (this->switchTimer != -1) {
                    Timer::getInstance()->deleteTimer(this->switchTimer);
                    this->switchTimer = -1;
                }
                Dispatcher::getInstance()->remListeners(this, LB_RAMP_2);
                new (this) Ramp_Exit(puck);
            }

            Ramp_Entry(PuckData* puck) : puck(puck) {
                cout << "*****************" << endl;
                cout << "IN RAMP_ENTRY" << endl;
                puck->printPuckData();
                cout << "*****************" << endl;

                puck->setState(TO_BE_DELETED);
                Dispatcher::getInstance()->remListeners(this, LB_RAMP_1);
                Dispatcher::getInstance()->addListeners(this, LB_RAMP_2);
                this->switchTimer = Timer::getInstance()->createTimer(Timer::RAMP, 1, 0, NULL, false, false);
                cout << "PSM: timer_create id:" << this->switchTimer << endl;
            }
        };
        /*
         *
         * RAMP_EXIT STATE
         *
         */
        struct Ramp_Exit : public State {
            PuckData* puck;
            // TODO exit
            Ramp_Exit(PuckData* puck) : puck(puck) {
#ifdef SYSTEM2
                SerialCom::getInstance()->sendMessage(SerialCom::READY2, 0);
#endif
                cout << "PUCKS_IN_RAMP****************************************" << endl;
                for (auto const& p : PuckData::pucks) {
                    p->state->currentPuck->printPuckData();
                }

                cout << "****************************************" << endl;
                Dispatcher::getInstance()->callListeners(PUCK_EXIT);

                cout << "PUCKS_OUT_RAMP****************************************" << endl;
                for (auto const& p : PuckData::pucks) {
                    p->state->currentPuck->printPuckData();
                }

                cout << "****************************************" << endl;

                cout << "PuckID=" << puck->GetID() << ", height=" << puck->GetHeight1()
                     << ", ticks=" << puck->getTicks() << ", type=" << ends;
                switch (puck->GetPucktype1()) {
                    case FLAT:
                        cout << "flat" << endl;
                        break;
                    case HOLE_WITHOUT_METAL:
                        cout << "hole without metal" << endl;
                        break;
                    case HOLE_WITH_METAL:
                        cout << "hole with metal" << endl;
                        break;
                    case UPSIDEDOWN:
                        cout << "upside down" << endl;
                        break;
                    default:
                        cout << "undefined" << endl;
                }
            }
        };
        /*
         *
         * ACCEPTED STATE
         *
         */
        struct Accepted : public State {
            PuckData* puck;

            virtual void switchTimeout() {
                Actuators::getInstance()->switchClose();
            }

            virtual void updateTimer() {

                puck->incrementTicks();
                if (puck->getTicks() > Constants::TIMER_EXIT_MAX) {
                    Dispatcher::getInstance()->callListeners(ERROR);
                    cout << "TIMER EXIT UPDATE ERROR " << endl;
                }
            }

            virtual void LB_Exit_1() {
#ifdef SYSTEM1
                if (puck->isChangedState()) {
                    puck->setChangedState(false);
                    if (PuckData::systemData.ready2) {
                        new (this) InTransfer(puck);
                    } else {
                        new (this) Motor1Stopped(puck);
                    }
                    // Actuators::getInstance()->motorStop();
                    cout << "PuckID=" << puck->GetID() << ", height=" << puck->GetHeight1()
                         << ", ticks=" << puck->getTicks() << ", type=" << ends;
                    switch (puck->GetPucktype1()) {
                        case FLAT:
                            cout << "flat" << endl;
                            break;
                        case HOLE_WITHOUT_METAL:
                            cout << "hole without metal" << endl;
                            break;
                        case HOLE_WITH_METAL:
                            cout << "hole with metal" << endl;
                            break;
                        case UPSIDEDOWN:
                            cout << "upside down" << endl;
                            break;
                        default:
                            cout << "undefined" << endl;
                    }
                    Dispatcher::getInstance()->remListeners(this, LB_EXIT_1);
                } else {
                    cout << "Puck" << puck->GetID() << ": LB_Exit_1 Ticks " << puck->getTicks() << endl;
                }
#endif
#ifdef SYSTEM2
                new (this) ReadyToBeRemoved(puck);
#endif
            }

            Accepted(PuckData* puck) : puck(puck) {
                cout << "***ACCEPTED***" << endl;
                Actuators::getInstance()->switchOpen();
                PuckData::switchTimer =
                    Timer::getInstance()->createTimer(Timer::SWITCH, 1, 0, 0, false, false);
                Dispatcher::getInstance()->remListeners(this, LB_SWITCH_2);
                Dispatcher::getInstance()->addListeners(this, LB_EXIT_1);
            }
        };
#ifdef SYSTEM1
        /*
         *
         * MOTOR_1_STOPPED STATE
         *
         */
        struct Motor1Stopped : public State {
            PuckData* puck;

            virtual void sendReady2() {
                cout << "PSM: ready2, error flag=" << Dispatcher::error_flag << endl;
                if (!Dispatcher::error_flag) {
                    Actuators::getInstance()->motorStart(Actuators::FORWARDS, Actuators::FAST);
                    Timer::getInstance()->startAllTimers();
                    new (this) InTransfer(puck);
                }
            }

            virtual void updateTimer() {
                puck->incrementTicks();
                if (puck->getTicks() < 0) {
                    // TODO delete puck, error usw
                }
            }

            Motor1Stopped(PuckData* puck) : puck(puck) {
                cout << "***MOTOR_1_STOPPED***" << endl;
                Actuators::getInstance()->motorStop();
                Timer::getInstance()->stopAllTimers();
                Dispatcher::getInstance()->remListeners(this, LB_EXIT_1);
                Dispatcher::getInstance()->addListeners(this, SEND_READY_2);
                // TODO checkTimer
            }
        };
        /*
         *
         * IN_TRANSFER STATE
         *
         */
        struct InTransfer : public State {
            PuckData* puck;

            virtual void transferFinished() {
                PuckData::lastAcceptedPuck = PuckData::nextExpectedPuck;
                PuckData::systemData.ready2 = false;
                Dispatcher::getInstance()->remListeners(this, TRANSFER_FINISHED);
                Dispatcher::getInstance()->remListeners(this, UPDATE_TIMER);
                puck->setState(TO_BE_DELETED);
                Dispatcher::getInstance()->callListeners(PUCK_EXIT);
            }

            virtual void updateTimer() {
                puck->incrementTicks();
                if (puck->getTicks() > 500) {
                    Dispatcher::getInstance()->callListeners(ERROR);
                    puck->setState(TO_BE_DELETED);
                    Dispatcher::getInstance()->callListeners(PUCK_EXIT);
                }
            }

            virtual void LB_Exit_2() {
                SerialCom::getInstance()->sendMessage(SerialCom::READY1, 0);
                Dispatcher::getInstance()->addListeners(this, UPDATE_TIMER);
            }

            InTransfer(PuckData* puck) : puck(puck) {
                cout << "***IN_TRANSFER***" << endl;

                SerialCom::getInstance()->sendMessage(SerialCom::PUCK, puck);
                Dispatcher::getInstance()->remListeners(this, LB_EXIT_1);
                Dispatcher::getInstance()->addListeners(this, LB_EXIT_2);
                Dispatcher::getInstance()->addListeners(this, TRANSFER_FINISHED);
                Dispatcher::getInstance()->remListeners(this, UPDATE_TIMER);

                // TODO checkTimer
            }
        };
#endif

#ifdef SYSTEM2
        /*
         *
         * READY_TO_BE_REMOVED STATE
         *
         */
        struct ReadyToBeRemoved : public State {
            PuckData* puck;

            virtual void LB_Exit_2() {
                puck->setState(TO_BE_DELETED);
                Dispatcher::getInstance()->callListeners(PUCK_EXIT);
                SerialCom::getInstance()->sendMessage(SerialCom::READY2, 0);

                cout << "PuckID=" << puck->GetID() << ", height1=" << puck->GetHeight1()
                     << ", height2=" << puck->GetHeight2() << ", ticks=" << puck->getTicks()
                     << ", type=" << ends;

                switch (puck->GetPucktype1()) {
                    case FLAT:
                        PuckData::nextExpectedPuck = HOLE_WITHOUT_METAL;
                        cout << "flat" << endl;
                        break;
                    case HOLE_WITHOUT_METAL:
                        PuckData::nextExpectedPuck = HOLE_WITH_METAL;
                        cout << "hole without metal" << endl;
                        break;
                    case HOLE_WITH_METAL:
                        PuckData::nextExpectedPuck = FLAT;
                        cout << "hole with metal" << endl;
                        break;
                    default:
                        cout << "undefined" << endl;
                }
                Dispatcher::getInstance()->remListeners(this, LB_EXIT_2);
            }

            ReadyToBeRemoved(PuckData* puck) : puck(puck) {
                cout << "puck state: ready to be removed" << endl;
                Dispatcher::getInstance()->remListeners(this, LB_EXIT_1);
                Dispatcher::getInstance()->addListeners(this, LB_EXIT_2);
                Actuators::getInstance()->motorStop();
            }
        };
#endif
    };
#endif /* PUCKSTATEMACHINE_H_ */
