/*
 * SystemStateMachine.cpp
 *
 *  Created on: 17.05.2016
 *      Author: abp842
 */

#include "SystemStateMachine.h"

SystemStateMachine::SystemStateMachine() : state(new Init()) {
    cout << "SSM: timer_create id:" << puckTickTimer << endl;
}

SystemStateMachine::~SystemStateMachine() {
    cout << "SSM: timer_delete id:" << puckTickTimer << endl;
    Timer::getInstance()->deleteTimer(puckTickTimer);
    Dispatcher::getInstance()->remListeners(this->state, LB_ENTRY_1);
    Dispatcher::getInstance()->remListeners(this->state, LB_ENTRY_2);
    Dispatcher::getInstance()->remListeners(this->state, LB_HEIGHT_1);
    Dispatcher::getInstance()->remListeners(this->state, LB_HEIGHT_2);
    Dispatcher::getInstance()->remListeners(this->state, LB_SWITCH_1);
    Dispatcher::getInstance()->remListeners(this->state, LB_SWITCH_2);
    Dispatcher::getInstance()->remListeners(this->state, LB_RAMP_1);
    Dispatcher::getInstance()->remListeners(this->state, LB_RAMP_2);
    Dispatcher::getInstance()->remListeners(this->state, LB_EXIT_1);
    Dispatcher::getInstance()->remListeners(this->state, LB_EXIT_2);
    Dispatcher::getInstance()->remListeners(this->state, HEIGHT);
    Dispatcher::getInstance()->remListeners(this->state, TRANSFER_PUCK);
    Dispatcher::getInstance()->remListeners(this->state, TRANSFER_FINISHED);
    Dispatcher::getInstance()->remListeners(this->state, SEND_READY_1);
    Dispatcher::getInstance()->remListeners(this->state, SEND_READY_2);
    Dispatcher::getInstance()->remListeners(this->state, IS_METAL);
    Dispatcher::getInstance()->remListeners(this->state, PUCK_EXIT);
    Dispatcher::getInstance()->remListeners(this->state, RAMP_1_FULL);
    Dispatcher::getInstance()->remListeners(this->state, RAMP_1_ACCEPTING);
    Dispatcher::getInstance()->remListeners(this->state, RAMP_2_FULL);
    Dispatcher::getInstance()->remListeners(this->state, RAMP_2_ACCEPTING);
    Dispatcher::getInstance()->remListeners(this->state, TIMEOUT);
    Dispatcher::getInstance()->remListeners(this->state, UPDATE_TIMER);
    Dispatcher::getInstance()->remListeners(this->state, ERROR);
    Dispatcher::getInstance()->remListeners(this->state, ERROR_2);
    Dispatcher::getInstance()->remListeners(this->state, OK);
    Dispatcher::getInstance()->remListeners(this->state, OK_2);
    Dispatcher::getInstance()->remListeners(this->state, SWITCH_TIMEOUT);
    Dispatcher::getInstance()->remListeners(this->state, RESET_BUTTON);
    Dispatcher::getInstance()->remListeners(this->state, ERROR_GONE);
    Dispatcher::getInstance()->remListeners(this->state, SWITCH_TIMEOUT);
    Dispatcher::getInstance()->remListeners(this->state, GO_TO_IDLE);
}

int SystemStateMachine::puckIdCounter = 0;

timer_t SystemStateMachine::puckTickTimer =
    Timer::getInstance()->createTimer(Timer::PUCK_TICK, 0, Constants::PUCK_TICK_FAST, NULL, true, true);

int SystemStateMachine::incrementIdCounter() {
    return SystemStateMachine::puckIdCounter++;
}
