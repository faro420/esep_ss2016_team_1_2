/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   constans.h
 * Author: Stefan
 *
 * Created on 16. Mai 2016, 15:45
 */

#ifndef CONSTANTS_H
#define CONSTANTS_H
class Constants {
public:
    static int PUCK_OFFSET;

    static int FLAT_HEIGHT_MIN;
    static int FLAT_HEIGHT_MAX;

    static int HOLE_HEIGHT_MIN;
    static int HOLE_HEIGHT_MAX;

    static int TIMER_OFFSET;

    static int TIMER_HEIGHT_MIN;
    static int TIMER_HEIGHT_MAX;

    static int TIMER_SWITCH_MIN;
    static int TIMER_SWITCH_MAX;

    static int TIMER_EXIT_MIN;
    static int TIMER_EXIT_MAX;

    static int PUCK_TICK_FAST;
    static int PUCK_TICK_SLOW;
};

#endif /* CONSTANTS_H */
