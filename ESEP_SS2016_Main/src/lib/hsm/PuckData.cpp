/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
#include "PuckData.h"
#include "../Logger.h"

#include <iostream>

using namespace Logger;

PuckType PuckData::nextExpectedPuck = FLAT;
PuckType PuckData::lastAcceptedPuck = FLAT;
PuckData::SystemData PuckData::systemData = {false, false, false, true, false, false, false};

deque<PuckStateMachine*> PuckData::pucks;

PuckData::PuckData(int ID)
    : ID(ID), pucktype1(UNDEFINED), pucktype2(UNDEFINED), state(NO_STATE), height1(0), height2(0), ticks(0),
      changedState(false) {}

timer_t PuckData::switchTimer = -1;

PuckData::PuckData(int ID, PuckType type, uint16_t height1)
    : ID(ID), pucktype1(type), pucktype2(UNDEFINED), state(NO_STATE), height1(height1), height2(0), ticks(0),
      changedState(false) {}

PuckData::~PuckData() {}

void PuckData::printPuckData() {
    cout << "PuckData: " << endl
         << "\tID: " << ID << endl
         << "\tPuckType: " << pucktype1 << endl
         << "\tPuckState: " << state << endl
         << "\theight1: " << height1 << endl
         << "\theight2: " << height2 << endl
         << "\tticks: " << ticks << endl;
}
