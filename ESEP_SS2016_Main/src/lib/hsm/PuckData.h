/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   PuckData.h
 * Author: Stefan
 *
 * Created on 15. Mai 2016, 18:40
 */

#ifndef PUCKDATA_H
#define PUCKDATA_H
#include "../timer/Timer.h"
#include <deque>
#include <stdint.h>

class PuckStateMachine;
using namespace std;
//! Type of Puck

enum States { NO_STATE, HEIGHT_MEASUREMENT, SWITCH, EXIT, TO_BE_DELETED };
enum PuckType {
    UNDEFINED,
    FLAT,
    HOLE_WITHOUT_METAL,
    HOLE_WITH_METAL,
    UPSIDEDOWN,
};

class PuckData {
private:
    int ID;
    PuckType pucktype1;
    PuckType pucktype2;
    States state;
    uint16_t height1;
    uint16_t height2;
    uint16_t ticks;
    bool changedState;

public:
    static deque<PuckStateMachine*> pucks;
    static PuckType nextExpectedPuck;
    static PuckType lastAcceptedPuck;
    static timer_t switchTimer;

    typedef struct {
        bool ramp1Full;
        bool ramp2Full;
        bool ready1;
        bool ready2;
        bool estop_me;
        bool estop_other;
        bool estop_source;
    } SystemData;

    static SystemData systemData;

    void SetHeight2(uint16_t height2) {
        this->height2 = height2;
    }

    uint16_t GetHeight2() const {
        return height2;
    }

    void SetHeight1(uint16_t height1) {
        this->height1 = height1;
    }

    uint16_t GetHeight1() const {
        return height1;
    }

    void SetPucktype1(PuckType pucktype) {
        this->pucktype1 = pucktype;
    }

    PuckType GetPucktype1() const {
        return pucktype1;
    }

    void SetPucktype2(PuckType pucktype) {
        this->pucktype2 = pucktype;
    }

    PuckType GetPucktype2() const {
        return pucktype2;
    }

    void SetID(int ID) {
        this->ID = ID;
    }

    int GetID() const {
        return ID;
    }

    uint16_t getTicks() const {
        return ticks;
    }

    void incrementTicks() {
        this->ticks += 1;
    }

    void resetTicks() {
        this->ticks = 0;
    }
    //	operator std::string() const;
    void printPuckData();

    PuckData(int ID);
    PuckData(int ID, PuckType type, uint16_t height1);
    ~PuckData();

    States getState() const {
        return state;
    }

    void setState(States state) {
        this->state = state;
    }

    bool isChangedState() const {
        return changedState;
    }

    void setChangedState(bool changedState) {
        this->changedState = changedState;
    }
};
#endif /* PUCKDATA_H */
