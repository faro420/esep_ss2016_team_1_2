/*
 * Demultiplexer.cpp
 *
 *  Created on: 13.05.2016
 *      Author: abp842
 */
#include "Demultiplexer.h"
#include "../SerialCom.h"
#include "../hal/Actuators.h"
#include "../hal/Sensors.h"
#include "../hal/portmap.h"
#include "../timer/Timer.h"
#include "Dispatcher.h"
#include "TimeKeeper.h"
#include <bitset>
#include <iostream>

using namespace HAL;
using namespace std;
void Demultiplexer::getNextMessage() {
    struct _pulse pulse;
    Dispatcher* disp = Dispatcher::getInstance();
    Sensors* sensors = Sensors::getInstance();
    MsgReceivePulse(sensors->get_isrChannel(), &pulse, sizeof(struct _pulse), NULL);
    if (init) {
        return;
    }

    switch (pulse.code) {
        case INTERN: {
            clear();
            break;
        }
        case ISR: {
            if ((pulse.value.sival_int & LIGHTBARRIER_ENTRY_BIT) != 0 && !Dispatcher::error_flag) {
                if (!lb_entry) {
                    disp->callListeners(LB_ENTRY_1);
                    lb_entry = true;
                } else {
                    disp->callListeners(LB_ENTRY_2);
                    lb_entry = false;
                }
            }

            if ((pulse.value.sival_int & LIGHTBARRIER_HEIGHT_MEASUREMENT_BIT) != 0 &&
                !Dispatcher::error_flag) {
                if (!lb_height) {
                    TimeKeeper::getInstance()->callTimeout(TimeKeeper::TIMEOUT_HM);
                    disp->callListeners(LB_HEIGHT_1);
                    lb_height = true;
                } else {
                    disp->callListeners(LB_HEIGHT_2);
                    lb_height = false;
                }
            }
            if ((pulse.value.sival_int & HEIGHT_MEASUREMENT_BIT) != 0 && !Dispatcher::error_flag) {
                if (!height) {
                    disp->callListeners(HEIGHT);
                    height = true;
                } else {
                    height = false;
                }
            }
            if ((pulse.value.sival_int & LIGHTBARRIER_SWITCH_BIT) != 0 && !Dispatcher::error_flag) {
                if (!lb_switch) {
                    TimeKeeper::getInstance()->callTimeout(TimeKeeper::TIMEOUT_SWITCH);
                    disp->callListeners(LB_SWITCH_1);
                    lb_switch = true;
                } else {
                    disp->callListeners(LB_SWITCH_2);
                    lb_switch = false;
                }
            }
            if ((pulse.value.sival_int & METALL_DETECTOR_BIT) != 0 && !Dispatcher::error_flag) {
                if (!metall_detector) {
                    disp->callListeners(IS_METAL);
                    metall_detector = true;
                } else {
                    metall_detector = false;
                }
            }
            if ((pulse.value.sival_int & LIGHTBARRIER_RAMP_BIT) != 0 && !Dispatcher::error_flag) {
                if (!lb_ramp) {
                    disp->callListeners(LB_RAMP_1);
                    lb_ramp = true;
                } else {
                    disp->callListeners(LB_RAMP_2);
                    lb_ramp = false;
                }
            } else if ((pulse.value.sival_int & LIGHTBARRIER_RAMP_BIT) != 0 && Dispatcher::error_flag) {
                if (lb_ramp) {
                    // disp->callListeners(ERROR_GONE);
                    disp->callListeners(LB_RAMP_2);
                    lb_ramp = false;
                }
            }
            if ((pulse.value.sival_int & LIGHTBARRIER_EXIT_BIT) != 0 && !Dispatcher::error_flag) {
                if (!lb_exit) {
                    TimeKeeper::getInstance()->callTimeout(TimeKeeper::TIMEOUT_EXIT);
                    disp->callListeners(LB_EXIT_1);
                    lb_exit = true;
                } else {
                    disp->callListeners(LB_EXIT_2);
                    lb_exit = false;
                }
            }
            if ((pulse.value.sival_int & (BUTTON_START_BIT << NIBBLE)) != 0) {
                if (!button_start) {

                    cout << "****************************************" << endl;
                    for (deque<PuckStateMachine*>::iterator it = PuckData::pucks.begin();
                         it != PuckData::pucks.end(); ++it) {
                        (*it)->state->currentPuck->printPuckData();
                    }
                    cout << "****************************************" << endl;
                    cout << "start button pressed" << endl;
                    button_start = true;
                } else {
                    cout << "start button released" << endl;
                    button_start = false;
                }
            }
            if ((pulse.value.sival_int & (BUTTON_STOP_BIT << NIBBLE)) != 0) {
                if (!button_stop) {
                    Actuators::getInstance()->motorStop();
                    Actuators::getInstance()->switchClose();
                    cout << "stop button pressed" << endl;
                    button_stop = true;
                } else {
                    cout << "stop button released" << endl;
                    button_stop = false;
                }
            }
            if ((pulse.value.sival_int & (BUTTON_RESET_BIT << NIBBLE)) != 0) {
                if (!button_reset) {

                    disp->callListeners(RESET_BUTTON);
                    cout << "reset button pressed" << endl;

                    button_reset = true;
                } else {
                    cout << "reset button released" << endl;
                    button_reset = false;
                }
            }
            if ((pulse.value.sival_int & (BUTTON_E_STOP_BIT << NIBBLE)) != 0) {
                if (!estop_active) {
                    if (estopTimer == -1) {
                        estopTimer =
                            Timer::getInstance()->createTimer(Timer::RESET, 0, 100000000, NULL, false, false);
                    } else {
                        Timer::getInstance()->modifyTimer(estopTimer, 0, 100000000, false);
                    }

                    estop_active = true;
                }
            }
            break;
        }

        case SERIAL: {
            switch (pulse.value.sival_int) {
                case SerialCom::HEARTBEAT:
                    first_heartbeat = true;
                    break;
                case SerialCom::ESTOP_PRESSED:
                    PuckData::systemData.estop_other = true;
                    disp->callListeners(ESTOP);
                    break;
                case SerialCom::ESTOP_RELEASED:
                    PuckData::systemData.estop_other = false;
                    disp->callListeners(ESTOP);
                    break;
                case SerialCom::READY1:
                    disp->callListeners(SEND_READY_1);
                    // PuckData::systemData.ready1 = true;
                    break;
                case SerialCom::READY2:
                    disp->callListeners(SEND_READY_2);
                    // PuckData::systemData.ready2 = true;
                    break;
                case SerialCom::RAMP_1_FULL:
                    PuckData::systemData.ramp1Full = !PuckData::systemData.ramp1Full;

                    if (Dispatcher::error_flag && !PuckData::systemData.ramp1Full) {
                        disp->callListeners(ERROR_GONE);
                    }

                    if (PuckData::systemData.ramp1Full && PuckData::systemData.ramp2Full) {
                        disp->callListeners(ERROR);
                    }
                    break;
                case SerialCom::RAMP_2_FULL:
                    PuckData::systemData.ramp2Full = !PuckData::systemData.ramp2Full;

                    if (Dispatcher::error_flag && !PuckData::systemData.ramp2Full) {
                        disp->callListeners(ERROR_GONE);
                    }

                    if (PuckData::systemData.ramp1Full && PuckData::systemData.ramp2Full) {
                        disp->callListeners(ERROR);
                    }
                    break;
                case SerialCom::PUCK:
                    break;
                case SerialCom::SYNCHRONIZE_EXPECTED_PUCK:
                    break;
                case SerialCom::TRANSFER_FINISHED:
                    disp->callListeners(TRANSFER_FINISHED);
                    break;
                case SerialCom::METAL_DETECTED:
                    PuckData::nextExpectedPuck = FLAT;
                    break;
                case SerialCom::RESET:
                    disp->callListeners(RESET_BUTTON);
                    break;
                case SerialCom::ROLLBACK_EXPECTED_PUCK:
                    switch (PuckData::nextExpectedPuck) {
                        case FLAT:
                            PuckData::nextExpectedPuck = HOLE_WITH_METAL;
                            break;
                        case HOLE_WITHOUT_METAL:
                            PuckData::nextExpectedPuck = FLAT;
                            break;
                        case HOLE_WITH_METAL:
                            PuckData::nextExpectedPuck = HOLE_WITHOUT_METAL;
                            break;
                        default:
                            break;
                    }
                    break;
                default:
                    cout << "error" << endl;
            }
            break;
        }
        case TIMER: {
            switch (pulse.value.sival_int) {
                case Timer::HEARTBEAT:
                    if (first_heartbeat) {
                        cout << timestamp << "Heartbeat timeout" << endl;
                        disp->callListeners(ERROR);
                    }
                    break;
                case Timer::RAMP:
#ifdef SYSTEM1
                    PuckData::systemData.ramp1Full = true;
                    SerialCom::getInstance()->sendMessage(SerialCom::RAMP_1_FULL, NULL);

#endif
#ifdef SYSTEM2
                    PuckData::systemData.ramp2Full = true;
                    SerialCom::getInstance()->sendMessage(SerialCom::RAMP_2_FULL, NULL);
                    SerialCom::getInstance()->sendMessage(SerialCom::READY2, NULL);

#endif
                    disp->callListeners(PUCK_EXIT);
                    Actuators::getInstance()->setTrafficLightBlinkSpeed(Actuators::AMBER,
                                                                        Actuators::BLINK_FAST);

                    if (PuckData::systemData.ramp1Full && PuckData::systemData.ramp2Full) {
                        disp->callListeners(ERROR);
                    }
                    break;
                case Timer::SWITCH:
                    disp->callListeners(SWITCH_TIMEOUT);
                    break;
                case Timer::PUCK_TICK:
                    disp->callListeners(UPDATE_TIMER);
                    //  cout << timestamp << ": Tick" << endl;
                    break;
                case Timer::RESET:
                    if (!button_estop) {
                        PuckData::systemData.estop_me = true;
                        SerialCom::getInstance()->sendMessage(SerialCom::ESTOP_PRESSED, NULL);
                        button_estop = true;
                    } else {
                        PuckData::systemData.estop_me = false;
                        SerialCom::getInstance()->sendMessage(SerialCom::ESTOP_RELEASED, NULL);
                        button_estop = false;
                    }

                    disp->callListeners(ESTOP);
                    estop_active = false;
                    break;
                default:
                    cout << "error" << endl;
            }
            break;
        }

        default:
            cout << "error" << endl;
    }
}

void Demultiplexer::resetSystem() {
    Actuators::getInstance()->motorStop();
    Actuators::getInstance()->switchClose();
    while (!PuckData::pucks.empty()) {
        delete PuckData::pucks.front();
        PuckData::pucks.pop_front();
    }
    delete this->system;
    delete this->error;
    this->error = new ErrorStateMachine();
    this->system = new SystemStateMachine();
    PuckData::nextExpectedPuck = FLAT;
    PuckData::systemData.ramp1Full = false;
    PuckData::systemData.ramp2Full = false;
    PuckData::systemData.ready1 = false;
    PuckData::systemData.ready2 = true;
    PuckData::systemData.estop_me = false;
    PuckData::systemData.estop_other = false;

    Actuators::getInstance()->setTrafficLightBlinkSpeed(Actuators::GREEN, Actuators::BLINK_NONE);
    Actuators::getInstance()->setTrafficLightBlinkSpeed(Actuators::AMBER, Actuators::BLINK_NONE);
    Actuators::getInstance()->setTrafficLightBlinkSpeed(Actuators::RED, Actuators::BLINK_NONE);

    Actuators::getInstance()->trafficLightOn(Actuators::GREEN);
    Actuators::getInstance()->trafficLightOff(Actuators::AMBER);
    Actuators::getInstance()->trafficLightOff(Actuators::RED);

    first_heartbeat = false;
}

void Demultiplexer::execute(void*) {
    while (true) {
        getNextMessage();
    }
}

void Demultiplexer::shutdown() {}

Demultiplexer::Demultiplexer()
    : lb_entry(false), lb_height(false), height(false), lb_switch(false), metall_detector(false),
      lb_ramp(false), lb_exit(false), button_start(false), button_stop(false), button_reset(false),
      button_estop(false), estop_active(false), tick_timer(0), first_heartbeat(false),
      system(new SystemStateMachine()), error(new ErrorStateMachine()), estop(new EstopStateMachine()),
      estopTimer(-1), init(true) {}

Demultiplexer::~Demultiplexer() {
    // TODO Auto-generated destructor stub
}
