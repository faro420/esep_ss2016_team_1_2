
#ifndef TIMEKEEPER_H_
#define TIMEKEEPER_H_
class TimeKeeper {

public:
    enum Timeouts { TIMEOUT_HM, TIMEOUT_SWITCH, TIMEOUT_EXIT };

    static TimeKeeper* getInstance();
    bool checkTimeout();
    void callTimeout(Timeouts timeout);

private:
    static TimeKeeper* instance_;
    Timeouts currentTimeout_;

    TimeKeeper();
    ~TimeKeeper();
    TimeKeeper(const TimeKeeper& other);
    TimeKeeper& operator=(const TimeKeeper& other);
};
#endif
