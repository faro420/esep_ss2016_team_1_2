/**
 *	@file SerialCom.cpp
 *  @date 11.04.2016
 *  @version 1.0
 *  @author Darryl Imhof
 */
#include "SerialCom.h"
#include "Logger.h"
#include "crc32.h"
#include <fcntl.h>
#include <iostream>
#include <stdlib.h>

using namespace std;
using namespace Logger;

uint8_t SerialCom::startbyte = STARTBYTE;

SerialCom* SerialCom::instance_ = NULL;

SerialCom* SerialCom::getInstance() {
    static SerialCom instance_;

    return &instance_;
}

SerialCom::SerialCom() : heartbeat(-1) {

    fd = open("/dev/ser1", O_RDWR | O_NOCTTY);
    if (fd < 0) {
        cout << "cannot open device" << endl;
        // TODO should be replaced by proper error handling when integrated in the main assignment
        exit(-1);
    }

    tcflush(fd, TCIOFLUSH);

    tcgetattr(fd, &oldtio); /* save current serial port settings */
    tcgetattr(fd, &newtio); /* save current serial port settings */

    cfsetispeed(&newtio, BAUDRATE);
    cfsetospeed(&newtio, BAUDRATE);

    newtio.c_cflag &= ~CSIZE;
    newtio.c_cflag &= ~CSTOPB;
    newtio.c_cflag &= ~PARENB;
    newtio.c_cflag |= CS8;
    newtio.c_cflag |= CREAD;
    newtio.c_cflag |= CLOCAL;

    tcsetattr(fd, TCSANOW, &newtio);
}

SerialCom::~SerialCom() {
    Timer::getInstance()->stopTimer(heartbeat);
    // restore old port settings
    tcsetattr(fd, TCSANOW, &oldtio);
    if (close(fd) < 0) {
        // TODO should be replaced by proper error handling when integrated in the main assignment
        exit(-1);
    }
}
int SerialCom::sendPacket(Packet* packet) {
    uint32_t checksum = crc32(0, packet, sizeof(Packet));
    if (write(fd, &(SerialCom::startbyte), 1) != 1) {
        return -1;
    }
    if (write(fd, packet, sizeof(Packet)) != sizeof(Packet)) {
        return -1;
    }
    if (write(fd, &checksum, sizeof(uint32_t)) != sizeof(uint32_t)) {
        return -1;
    }
    return 0;
}

int SerialCom::recvPacket(Packet* packet) {
    uint8_t buf = 0;
    uint32_t checksum = 0;
    // read bytes until the startbyte 0xaa is found
    readcond(fd, &buf, 1, 1, 0, 0);
    if (buf != STARTBYTE) {
        return -1;
    }

    // read packet
    if (readcond(fd, packet, sizeof(Packet), sizeof(Packet), 0, 0) <= 0) {
        return -1;
    }

    // read and compare transmitted checksum with calculated
    readcond(fd, &checksum, sizeof(uint32_t), sizeof(uint32_t), 0, 0);
    if (checksum != crc32(0, packet, sizeof(Packet))) {
        return -1;
    }
    return 0;
}

void SerialCom::sendMessage(PacketType type, PuckData* data) {
    SerialCom::Packet p;

    p.packetType = type;
    if (type == PUCK || type == SYNCHRONIZE_EXPECTED_PUCK) {
        SerialCom::Puck puck;
        puck.height = data->GetHeight1();
        puck.puckType = data->GetPucktype1();
        p.puck = puck;
    } else if (type == HEARTBEAT) {
        if (heartbeat == -1) {
            heartbeat = Timer::getInstance()->createTimer(Timer::HEARTBEAT, 0, 50000000, 0, false, false);
        } else {
            Timer::getInstance()->modifyTimer(heartbeat, 0, 50000000, false);
        }
    } else {
        cout << timestamp << " sending " << type << endl;
    }

    if (SerialCom::sendPacket(&p) == -1) {
        cout << "Error while sending Packet" << endl;
    }
}
