/**
 * This class represents one object which goes through an HSM.
 * @author Stefan Sylvius Wagner
 * @date 26.04.2016
 */
#include "Dispatcher.h"
#include "Transitions.h"
#include <iostream>

using namespace std;
class Context {
private:
    struct State : public Transitions{virtual void lbHeight(){} virtual void lbSwitch(){} virtual void
                                          lbRamp1Full(){} virtual void lbOut(){}} *
                   state;

    struct StateEntry1 : public State {
        virtual void lbHeight() {
            cout << "Entry1 -> HM1" << endl;
            // Transitions for state
            Dispatcher* disp = Dispatcher::getInstance();
            disp->remListeners(this, LB_HEIGHT);
            disp->addListeners(this, LB_SWITCH);

            new (this) StateHeightMeauserment1;
        }
    };

    struct StateHeightMeauserment1 : public State {
        virtual void lbSwitch() {
            cout << "HM1 -> Ramp1" << endl;

            Dispatcher* disp = Dispatcher::getInstance();
            disp->remListeners(this, LB_SWITCH);
            disp->addListeners(this, LB_RAMP1FULL);
            disp->addListeners(this, LB_OUT);

            new (this) StateRamp1;
        }
    };

    struct StateRamp1 : public State {
        virtual void lbRamp1Full() {
            cout << "Ramp1 -> Ramp1Full" << endl;
            new (this) StateRamp1Full;
        }
        virtual void lbOut() {
            cout << "Ramp1 -> Exit1" << endl;
            new (this) StateExit1;
        }
    };

    struct StateRamp1Full : public State { /*ToDo*/
    };
    struct StateExit1 : public State { /*ToDo*/
    };

public:
    Context() : state(new StateEntry1()) {
        Dispatcher* disp = Dispatcher::getInstance();
        disp->addListeners(this->state, LB_HEIGHT);
    }
    ~Context() {}
    //    void lbHeight(){state->lbHeight();}
    //    void lbSwitch(){state->lbSwitch();}
    //    void lbRamp1Full(){state->lbRamp1Full();}
    //    void lbOut(){state->lbOut();}
};
