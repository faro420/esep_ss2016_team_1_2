/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   main.cpp
 * Author: Stefan
 *
 * Created on 26. April 2016, 02:05
 */

#include "Context.cpp"
#include <cstdlib>

using namespace std;

/*
 *
 */
int main(int argc, char** argv) {
    Dispatcher* dispatcher = Dispatcher::getInstance();

    Context con;
    dispatcher->callListeners(LB_HEIGHT);
    dispatcher->callListeners(LB_SWITCH);

    Context con2;
    dispatcher->callListeners(LB_HEIGHT);
    dispatcher->callListeners(LB_RAMP1FULL);
    return 0;
}
