/* 
 * @file    main.cpp
 * @author  Simon Brummer
 * @version 0.2
 * @desc    Demoprojekt fuer das erste SE2 Tutorium.
 *          Das Projekt darf gerne die Basis fuer euer eigentliches Projekt sein
 *          da, hier bereits alles Grundlegende Konfiguriert ist.
 */

/*
 * Further development by ESEP_SS2016_TEAM_1_2
 * Members: Stefan Wagner, Farshid Baha, Dennis Kirsch, Darryl Imhof
 * @version 1.0
 * @desc Demo project for the first milestone. Usage of the Traffic Light IO.
 *
 *
 */

//#define SIMULATION  // #define Auskommentieren falls mit der Simulation gearbeitet wird.

#include <cstdlib>
#include <iostream>
#include "Blink_Thread.h"
#include "lib/HWaccess.h"

using namespace std;

int main(int argc, char *argv[]) {

    // Baut Verbindung zu Simulation auf
    #ifdef SIMULATION
        IOaccess_open();
    #endif

    cout << "First QNX Demo, let there be light!." << endl;

    Blink_Thread th1(10); // Thread 1, soll 5 mal blinken

    th1.start(NULL);     // Start Thread 1



    th1.join();          // Warten auf das Ende von Thread 2

    #ifdef SIMULATION
       IOaccess_close();
    #endif

    return EXIT_SUCCESS;
}
