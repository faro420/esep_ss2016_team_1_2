# Project ESEP SS2016 for SE2
![IMG_20160525_165720.jpg](https://bitbucket.org/repo/KpR7Md/images/2158352927-IMG_20160525_165720.jpg)

## Short project description:

Elements are transported in a given order defined by requirements via two connected assembly lines. Each of them is controlled by an extern QNX driven computer. Both communicate with each other by utilizing a serial bus.

## Current Status:

Software Version : 1.0

Document Version : 1.2

The software passed our integration tests and is ready to be reviewed by our supervisor. Last changes added error handling and detection.  

## Contact:

Name                  | Matr-Nr | Email
:-------------------- | ------- | :--------------------------------------------------------------------------------
Stefan Sylvius Wagner | 2103833 | [stefansylvius.wagner@haw-hamburg.de](mailto:stefansylvius.wagner@haw-hamburg.de)
Mir Farshid Baha      | 2141801 | [mirfarshid.baha@haw-hamburg.de](mailto:mirfarshid.baha@haw-hamburg.de)
Dennis Kirsch         | 2165327 | [dennis.kirsch@haw-hamburg.de](mailto:dennis.kirsch@haw-hamburg.de)
Darryl Imhof          | 2143046 | [darryl.imhof@haw-hamburg.de](mailto:darryl.imhof@haw-hamburg.de)
Diana Haidarbaigi     | 2160484 | [diana.haidarbaigi@haw-hamburg.de](mailto:diana.haidarbaigi@haw-hamburg.de)
